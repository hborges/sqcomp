/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import com.csvreader.CsvReader;
import com.csvreader.CsvWriter;
import com.stromberglabs.cluster.Cluster;

/**
 *
 * @author hudson
 */
public class FileController {

    /*
     * Metodo que lê um arquivo .csv contendo os dados de similaridade e retorna
     * em um array de Objects de tamanho 2 No primeiro registro contem uma lista
     * (List<String>) contendo os vertices No segundo registro é retornado um
     * lista (List<String[]>) que contem os pares de vertices, ou seja, a aresta
     */
    public static Object[] getArestasVerticesFromCSV(File arquivo) throws FileNotFoundException, IOException {

        CsvReader reader = new CsvReader(arquivo.getAbsolutePath());
        reader.setDelimiter(';');
        reader.setTrimWhitespace(true);

        List<String> vertices = new ArrayList<String>();
        List<String[]> arestas = new ArrayList<String[]>();

        reader.readHeaders();
        String[] headers = reader.getHeaders();
        for (int i = 1; i < headers.length; i++) {
            headers[i] = headers[i].substring(0, headers[i].indexOf('.'));
            vertices.add(headers[i]);
        }
        int cont = 1;
        while (reader.readRecord()) {
            String[] record = reader.getValues();
            for (int i = 1; i < record.length; i++) {
                if (!record[i].trim().equalsIgnoreCase("0")) {
                    arestas.add(new String[]{headers[cont], headers[i]});
                }
            }
            cont++;
        }

        return new Object[]{vertices, arestas};

    }

    public static void savePrimaryOrSecundaryCluster(Map<String, Cluster> map, String nomePlanilha) throws IOException {

        File file = new File(nomePlanilha);

        CsvWriter writer = new CsvWriter(file.getAbsolutePath());
        writer.setDelimiter(';');
        
        writer.writeRecord(new String[]{"Sistema", "Cluster1", "Cluster2", "Cluster3", "Cluster4", "Cluster5"});

        for (String s : map.keySet()) {
            float[] location = map.get(s).getLocation();
            Arrays.sort(location);
            writer.write(s);
            for (float f : location) {
                writer.write(Float.toString(f));
            }
            writer.endRecord();
        }

        writer.flush();
        writer.close();
        
    }

    public static void saveMatrix(List<Object[]> list, String nomePlanilha) throws IOException {
        saveCluster(list, nomePlanilha);
    }

    private static void saveCluster(List<Object[]> list, String nomePlanilha) throws IOException {

        File file = new File(nomePlanilha);

        CsvWriter writer = new CsvWriter(file.getAbsolutePath());
        writer.setDelimiter(';');

        for (Object[] item : list) {
            for (Object i : item) {
                writer.write(i.toString());
            }
            writer.endRecord();
        }

        writer.flush();
        writer.close();
    }
}
