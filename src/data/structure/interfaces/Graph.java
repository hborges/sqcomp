package data.structure.interfaces;

import java.util.List;
import java.util.Set;

import data.structure.exceptions.EdgeException;
import data.structure.exceptions.VertexException;

public interface Graph<V> {

	public abstract void addVertex(V v, String label) throws VertexException;

	public abstract void addEdge(V v1, V v2) throws EdgeException;

	public abstract boolean isNeighbor(V v1, V v2);

	public abstract Set<V> getVertices();

	public abstract List<V> getNeighbors(V v);

}