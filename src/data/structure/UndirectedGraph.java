package data.structure;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import data.structure.model.Vertex;

import data.structure.exceptions.EdgeException;
import data.structure.exceptions.VertexException;
import data.structure.interfaces.Graph;

public class UndirectedGraph<V> implements Graph<V> {
	
	private Map<V,ArrayList<V>> graph;
	private Map<V,Vertex<V>> map_vertices;
	
	public UndirectedGraph() {
		this.graph = new HashMap<V,ArrayList<V>>();
		this.map_vertices = new HashMap<V,Vertex<V>>();
	}

	@Override
	public void addVertex(V v,String label) throws VertexException{
		Vertex<V> _v = new Vertex<V>(v,label);
		if(this.map_vertices.containsKey(v)){throw new VertexException();}
		this.map_vertices.put(v,_v);
		this.graph.put(v, new ArrayList<V>());
	}

	@Override
	public void addEdge(V v1,V v2) throws EdgeException{
		if(this.map_vertices.containsKey(v1) && this.map_vertices.containsKey(v2)){
			this.graph.get(v1).add(v2);
			this.graph.get(v2).add(v1);
		}else{
			throw new EdgeException("Inexistent vertices");
		}
		
	}
	
	@Override
	public boolean isNeighbor(V v1, V v2){
		if(graph.get(v1).contains(v2)){
			return true;
		}else{
			return false;
		}
	}
	
	@Override
	public Set<V> getVertices(){
		return map_vertices.keySet();
	}
	
	@Override
	public List<V> getNeighbors(V v){
		return graph.get(v);
	}

}
