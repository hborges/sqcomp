package data.structure.model;

public class Vertex<V> implements Comparable<Vertex<V>>{
	
	private String label;
	private V content;
	
	public Vertex(V content){
		this.content = content;
	}
	
	public Vertex(V content,String label){
		this.content = content;
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public V getContent() {
		return content;
	}
	public void setContent(V content) {
		this.content = content;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		Vertex<V> _o = (Vertex<V>) obj;
		if(_o.content == this.content){
			return true;
		}else{
			return false;
		}
	}

	@Override
	public int compareTo(Vertex<V> o) {
		if(this.content.equals(o.content)){
			return 0;
		}else{
			return this.label.compareTo(o.label);
		}
	}

}
