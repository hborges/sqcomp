package data.structure.exceptions;

@SuppressWarnings("serial")
public class EdgeException extends Exception {

	public EdgeException(String message){
		super(message);
	}
	
	public EdgeException(){
		super("This edge has already been added");
	}
}
