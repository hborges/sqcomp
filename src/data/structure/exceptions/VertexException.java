package data.structure.exceptions;

@SuppressWarnings("serial")
public class VertexException extends Exception {

	public VertexException(String message){
		super(message);
	}
	
	public VertexException(){
		super("This vertex has already been added");
	}
}
