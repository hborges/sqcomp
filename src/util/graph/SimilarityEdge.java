package util.graph;

import org.jgraph.graph.DefaultEdge;

public class SimilarityEdge<V> extends DefaultEdge {

	private static final long serialVersionUID = -8342279642909740410L;
	
	private int numSimilarMetrics;
	private Object[] similarMetrics;
	
	public SimilarityEdge(V source, V target, int numSimilarMetrics, Object[] similarMetrics) {
		this.numSimilarMetrics = numSimilarMetrics;
		this.similarMetrics = similarMetrics;
	}
	
	public int getNumSimilarMetrics() {
		return this.numSimilarMetrics;
	}
	
	public Object[] getSimilarMetrics() {
		return this.similarMetrics;
	}
	
	// m�todo que insere o label na visualiza��o da aresta
//	@Override
//	public String toString() {
//		return this.getLabel();
//	}
}