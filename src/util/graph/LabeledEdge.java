package util.graph;

import org.jgraph.graph.DefaultEdge;

public class LabeledEdge<V> extends DefaultEdge {

	private static final long serialVersionUID = -8342279642909740410L;
	
	private String label;
	
	public LabeledEdge(V source, V target, String label) {
		setLabel(label);
	}
	
	public String getLabel() {
		return this.label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}
	
	@Override
	public String toString() {
		return this.getLabel();
	}
}