package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class Properties {

	private static final String PROPERTIES_FILE = "project.properties";

	private static InputStream propertiesInputStream = null;
	private static java.util.Properties prop = new java.util.Properties();

	// Propriedades do arquivo
	public static final String WORKSPACE = "workspace";
	
	// Propriedades gerais
	public static final String MSE_OUTPUT = "mse";
	public static final String METRICS_OUTPUT = "metricas";
	public static final String CLUSTERING_OUTPUT = "cluster";
	public static final String CLUSTERING_PRIMARY_OUTPUT = "primario";
	public static final String CLUSTERING_SECONDARY_OUTPUT = "secundario";
	public static final String CLUSTERING_MATRICES_OUTPUT = "matrizes";
	

	public static void carregar() {
		try {
			File propertiesFile = new File(PROPERTIES_FILE);
			propertiesInputStream = new FileInputStream(propertiesFile);
			if (prop.isEmpty() && propertiesFile.exists()) {
				prop.load(propertiesInputStream);
			}
		} catch (FileNotFoundException e) {
		} catch (IOException e) {
		} finally {
			try {
				if (propertiesInputStream != null)
					propertiesInputStream.close();
			} catch (IOException e) {
			}
		}
	}
	
	public static String getProperty(String property) {
		String value = prop.getProperty(property);
		return value != null ? value.trim() : null;
	}

	public static void setProperty(String property, String value) {
		prop.setProperty(property, value);
		
		try {
			FileOutputStream outputStream = new FileOutputStream(new File(PROPERTIES_FILE));
			prop.store(outputStream, null);
			outputStream.close();
		} catch (IOException e) {
		}
	}
}