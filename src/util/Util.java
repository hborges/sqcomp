/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package util;

import java.awt.Component;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.JFileChooser;

import com.csvreader.CsvReader;

import data.FileController;

/**
 *
 * @author Hudson
 */
public class Util {

    public static int linearSearch(Object[] array, Object value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(value))
                return i;
        }
        return -1;
    }

    public static void CSVMetricsAnalisys(Component parent) throws FileNotFoundException, IOException {

        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        chooser.setMultiSelectionEnabled(true);

        chooser.setFileFilter(new javax.swing.filechooser.FileFilter() {

            @Override
            public boolean accept(File f) {
                return f.getName().toLowerCase().endsWith(".csv") || f.isDirectory();
            }

            @Override
            public String getDescription() {
                return "Arquivos MSE (.csv)";
            }
        });

        File[] fontes = null;


        if (chooser.showDialog(parent, "Open") != JFileChooser.CANCEL_OPTION) {

            fontes = chooser.getSelectedFiles();
            CsvReader csvReader = null;

            for (File f : fontes) {

                csvReader = new CsvReader(f.getAbsolutePath(), ';');
                csvReader.readHeaders();

                Map<String, Integer> map = new TreeMap<String, Integer>();
                String value = null;
                while (csvReader.readRecord()) {
                    value = csvReader.get(0).concat(";").concat(csvReader.get(1));
                    if (!map.containsKey(value)) {
                        map.put(value, 1);
                    } else {
                        Integer i = map.remove(value);
                        map.put(value, ++i);
                    }
                }

                List<Object[]> output = new ArrayList<Object[]>();
                for (String s : map.keySet()) {
                    if (map.get(s) > 1) {
                        output.add(new Object[]{map.get(s), s.subSequence(0, s.indexOf(";")), s.subSequence(s.indexOf(";") + 1, s.length())});
                    }
                }

                if (!output.isEmpty()) {
                    FileController.saveMatrix(output, f.getAbsolutePath().concat(".analisys.csv"));
                }

                csvReader.close();

            }

        }

    }

}
