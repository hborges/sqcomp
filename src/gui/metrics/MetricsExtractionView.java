/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ExtrairMetricas.java
 *
 * Created on Jan 20, 2012, 8:12:36 AM
 */
package gui.metrics;

import java.io.File;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import util.Properties;
import control.MSEController;
import control.MetricsController;

/**
 *
 * @author hudson, gustavojss
 */
public class MetricsExtractionView extends javax.swing.JInternalFrame {

	private static final long serialVersionUID = 5028638938546848162L;
	
	private boolean stopExtraction;
	private Thread mseControllerThread, metricsControllerThread;
	
	private File[] pathFontes;
	private Vector<File> projectFolders;
	
	private MSEController mseController;
    private MetricsController metricsController;
    
    private String mseOutputFolder;
    private String metricsOutputFolder;

    public MetricsExtractionView() {
        initComponents();
        initListeners();
        
        this.stopExtraction = false;
        
        String workspaceFolder = Properties.getProperty(Properties.WORKSPACE) + File.separator; 
        this.mseOutputFolder = workspaceFolder + Properties.MSE_OUTPUT;
        this.metricsOutputFolder = workspaceFolder + Properties.METRICS_OUTPUT;
    }

    @SuppressWarnings("rawtypes")
	private void initComponents() {

    	setClosable(true);
    	setMaximizable(true);
    	setResizable(true);
    	setTitle("Source Code Metric Extraction");
    	setVisible(true);
    	try {
    		setSelected(true);
    	} catch (java.beans.PropertyVetoException e1) {
    		e1.printStackTrace();
    	}
    	
        jPanel1 = new javax.swing.JPanel();
        selecionarProjetosButton = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        listProjects = new javax.swing.JList();
        executarExtracaoButton = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        extracaoOutput = new javax.swing.JTextArea();
        progresso = new javax.swing.JProgressBar();

        selecionarProjetosButton.setText("Select");
        executarExtracaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/start.png")));
        executarExtracaoButton.setText("Start");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true), " System Selection ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 12))); // NOI18N
        jScrollPane1.setViewportView(listProjects);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                        .addGap(119, 119, 119)
                        .addComponent(selecionarProjetosButton, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(executarExtracaoButton, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(selecionarProjetosButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(executarExtracaoButton)
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true), " Processing Progress ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("DejaVu Sans", 1, 12))); // NOI18N

        jScrollPane2.setAutoscrolls(true);

        extracaoOutput.setColumns(20);
        extracaoOutput.setEditable(false);
        extracaoOutput.setRows(5);
        jScrollPane2.setViewportView(extracaoOutput);

        progresso.setStringPainted(true);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 408, Short.MAX_VALUE)
                    .addComponent(progresso, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 408, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(progresso, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        java.awt.Dimension dialogSize = getSize();
        setLocation((screenSize.width-dialogSize.width)/2,(screenSize.height-dialogSize.height)/2);
        
    }
    
    private void initListeners() {
    	selecionarProjetosButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                selecionarProjetosActionPerformed(evt);
            }
        });
    	
    	executarExtracaoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                executarExtracaoActionPerformed(evt);
            }
        });
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	private void selecionarProjetosActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	JFileChooser chooser = new JFileChooser();
    	chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setMultiSelectionEnabled(true);
        
        if (chooser.showDialog(this, "Open") != JFileChooser.CANCEL_OPTION) {
            this.projectFolders = new Vector();
            for (File f : chooser.getSelectedFiles()) {
                this.projectFolders.addElement(f);
            }
            this.listProjects.setListData(this.projectFolders);
            this.listProjects.setSelectionInterval(0, this.projectFolders.size());
        }
    }

	private void executarExtracaoActionPerformed(java.awt.event.ActionEvent evt) {
    	
    	if (!System.getProperty("os.name").toLowerCase().contains("linux") && !System.getProperty("os.name").toLowerCase().contains("windows")) {
            JOptionPane.showMessageDialog(this, "For software restrictions, the metrics extraction shall only be executed on Linux or Windows", "Error", JOptionPane.ERROR_MESSAGE);
        } else if (!stopExtraction) {
        	
        	File mseFolder = new File(mseOutputFolder);
        	if (!mseFolder.exists()) mseFolder.mkdirs();
        	
        	File metricsFolder = new File(metricsOutputFolder);
        	if (!metricsFolder.exists()) metricsFolder.mkdirs();
        	
            try {
            	
            	// ----------------------- Extraindo arquivos .mse -----------------------
                this.mseController = new MSEController(mseOutputFolder, projectFolders.toArray(new File[0]));

                executarExtracaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/wait.gif")));
                executarExtracaoButton.setText("Executing");
                executarExtracaoButton.setEnabled(false);
                
                mseControllerThread = new Thread(mseController);
                mseControllerThread.start();

                new Thread(new PanelUpdator()).start();
                
                // --------------------- Extraindo m�tricas com Moose ---------------------
                // Cria��o de uma thread que aguarda a extra��o dos arquivos .mse para extrair as m�tricas
                new Thread(new MSEControllerListener()).start();

                this.repaint();
                
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e.getLocalizedMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
    
    private class PanelUpdator implements Runnable {
    	
    	private double progressValue;

        @Override
        public void run() {
        	
        	final int TOTAL_EXTRACOES = mseController.getNumProjetos() * 2;
        	
            while (!stopExtraction) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MetricsExtractionView.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                // Atualizando barra de progresso
                int projetosAnalisados = mseController.getAnalisados();
                projetosAnalisados += (metricsController != null) ? metricsController.getAnalisados() : 0;
                
                progressValue = ((double) projetosAnalisados / TOTAL_EXTRACOES) * 100;
                progresso.setValue((int) progressValue);
                
                // Atualizando sa�da dos m�todos
                String output = mseController.getOutput();
                output += (metricsController != null) ? metricsController.getOutput() : "";
                
                extracaoOutput.setText(output);
                
                stopExtraction = (mseControllerThread != null && !mseControllerThread.isAlive()) 
            			&& (metricsControllerThread != null && !metricsControllerThread.isAlive());
            }
            
            if (!mseController.getErros().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (File f : mseController.getErros()) {
                    sb.append("* " + f.getName());
                    sb.append("\n");
                }
                JOptionPane.showMessageDialog(null, "Problem occurred in the following systems:\n " + sb.toString() + "Please check output logs!", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
            if (metricsController != null && !metricsController.getErros().isEmpty()) {
                StringBuilder sb = new StringBuilder();
                for (File f : metricsController.getErros()) {
                    sb.append("* " + f.getName());
                    sb.append("\n");
                }
                JOptionPane.showMessageDialog(null, "Problem occurred in the following systems:\n " + sb.toString() + "Please check output logs!", "Error", JOptionPane.ERROR_MESSAGE);
            }
            
            executarExtracaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/start.png")));
            executarExtracaoButton.setText("Start");
            executarExtracaoButton.setEnabled(true);
        }
    }
    
    private class MSEControllerListener implements Runnable {
		@Override
		public void run() {
			while (!stopExtraction) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(MetricsExtractionView.class.getName()).log(Level.SEVERE, null, ex);
                }
                
                // Se todos os projetos foram extra�dos para arquivos .mse
                if (mseController.getAnalisados() == mseController.getNumProjetos()) {
                	if (metricsControllerThread == null) {
                		pathFontes = new File(mseOutputFolder).listFiles();
                		
                        metricsController = new MetricsController(new File(metricsOutputFolder), pathFontes);
                        metricsControllerThread = new Thread(metricsController);
                        metricsControllerThread.start();
                    }                	
                }
            }
		}
    }
    
    @SuppressWarnings("rawtypes")
    private javax.swing.JList listProjects;
    private javax.swing.JButton executarExtracaoButton;
    private javax.swing.JButton selecionarProjetosButton;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JProgressBar progresso;
    private javax.swing.JTextArea extracaoOutput;
    
}