package gui.cluster;

import java.io.File;
import java.io.FileFilter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import util.Properties;
import control.cluster.ClusteringController;

/**
 *
 * @author hudson, gustavojss,paloma
 */
public class ClusteringView extends javax.swing.JInternalFrame {

	private static final long serialVersionUID = 148033325706574967L;
	
	private Thread thread;
    private ClusteringController controller;
    private Set<Integer> selectedMetrics;
    
    private String clusteringInputFolder;
    private String clusteringPrimaryFolder, clusteringSecondaryFolder, clusteringMatricesFolder;
    
    public int getValorNormalizacao() {
		return valorNormalizacao;
	}

	public void setValorNormalizacao(int valorNormalizacao) {
		this.valorNormalizacao = valorNormalizacao;
	}
	private int valorNormalizacao;

    public ClusteringView() {
        initComponents();
        initListeners();
        
        String workspaceFolder = Properties.getProperty(Properties.WORKSPACE) + File.separator; 
        this.clusteringInputFolder = workspaceFolder + Properties.METRICS_OUTPUT;
        retrieveProjects();
        
        this.pack();
        this.selectedMetrics = new HashSet<Integer>();
        
        String clusteringOutputFolder = workspaceFolder + Properties.CLUSTERING_OUTPUT + File.separator;
        this.clusteringPrimaryFolder = clusteringOutputFolder + Properties.CLUSTERING_PRIMARY_OUTPUT;
        this.clusteringSecondaryFolder = clusteringOutputFolder + Properties.CLUSTERING_SECONDARY_OUTPUT;
        this.clusteringMatricesFolder = clusteringOutputFolder + Properties.CLUSTERING_MATRICES_OUTPUT;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void initComponents() {

    	setTitle("System Clustering");
    	setClosable(true);
    	
        jPanel1 = new javax.swing.JPanel();
        projetos_nao_selecionados = new javax.swing.JScrollPane();
        nonSelectedProjects = new javax.swing.JList(new DefaultListModel());
        nonSelectedProjects.setToolTipText("This is the list of systems presented in \"metricas\" in your workspace folder, in CSV files");
        projetos_selecionados = new javax.swing.JScrollPane();
        selectedProjects = new javax.swing.JList(new DefaultListModel());
        lista_de_metricas = new javax.swing.JScrollPane();
        
        addAllProjectsButton = new javax.swing.JButton(">>");
        addProjectButton = new javax.swing.JButton(">");
        removeProjectButton = new javax.swing.JButton("<");
        removeAllProjectsButton = new javax.swing.JButton("<<");
        
        executarClusterizacaoButton = new javax.swing.JButton("Run");
        executarClusterizacaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/start.png")));
        panel_metricas = new javax.swing.JPanel();
        
        allM = new javax.swing.JCheckBox("Select All");
        loc = new javax.swing.JCheckBox("LoC (Lines of COde)");
        nom = new javax.swing.JCheckBox("NoM (Number of Methods)");
        pubm = new javax.swing.JCheckBox("PubM (Public Methods)");
        prim = new javax.swing.JCheckBox("PriM (Private Methods)");
        noa = new javax.swing.JCheckBox("NoA (Number of Attributes)");
        puba = new javax.swing.JCheckBox("PubA (Public Attributes)");
        pria = new javax.swing.JCheckBox("PriA (Private Attributes)");
        noah = new javax.swing.JCheckBox("NoAH (Inherited Attributes)");
        nomh = new javax.swing.JCheckBox("NoMH (Inherited Methods)");
        
        fin = new javax.swing.JCheckBox("Fan-In");
        fout = new javax.swing.JCheckBox("Fan-Out");
        nomo = new javax.swing.JCheckBox("NomO (Number Of Methods Overriden)");
        noc = new javax.swing.JCheckBox("Noc (Number Of Children)");
        tnoc = new javax.swing.JCheckBox("TnoC (Total Number Of Children)");
        woc = new javax.swing.JCheckBox("WoC (Weight Of A Class)");
        wmc = new javax.swing.JCheckBox("WMC (Weighted Method Count)");
        cbc = new javax.swing.JCheckBox("CBO (Coupling Between Classes)");
        locm = new javax.swing.JCheckBox("Lcom (Lack Of Cohesion In Methods)");
        rfc = new javax.swing.JCheckBox("RFC (Response For Class)");
        hnl = new javax.swing.JCheckBox("HNL (Hierarchy Nesting Level)");
                
        
        String tooltip = "This value determines from many metrics two systems are considered similar to each other.";
        label_value = new javax.swing.JLabel("Qmin");
        combo_valor_normalizacao = new javax.swing.JComboBox();
        label_value.setToolTipText(tooltip);
        combo_valor_normalizacao.setToolTipText(tooltip);
        
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        setVisible(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(
        		new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true), " System Selection "));

        projetos_nao_selecionados.setViewportView(nonSelectedProjects);
        projetos_selecionados.setViewportView(selectedProjects);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(projetos_nao_selecionados, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addAllProjectsButton, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                    .addComponent(addProjectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(removeProjectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(removeAllProjectsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(projetos_selecionados, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {projetos_nao_selecionados, projetos_selecionados});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(projetos_nao_selecionados)
                    .addComponent(projetos_selecionados)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(addAllProjectsButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addProjectButton)
                        .addGap(73, 73, 73)
                        .addComponent(removeProjectButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(removeAllProjectsButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        panel_metricas.setBorder(javax.swing.BorderFactory.createTitledBorder(
        		new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true), " Metrics Selection "));

        combo_valor_normalizacao.setModel(new javax.swing.DefaultComboBoxModel(new Integer[] {}));
        combo_valor_normalizacao.setEnabled(false);

        javax.swing.GroupLayout layout_panel_metricas = new javax.swing.GroupLayout(panel_metricas);
        panel_metricas.setLayout(layout_panel_metricas);
        
        layout_panel_metricas.setHorizontalGroup(
            layout_panel_metricas.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout_panel_metricas.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout_panel_metricas.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout_panel_metricas.createSequentialGroup()
                        .addComponent(allM)
                        .addContainerGap())
                    .addGroup(layout_panel_metricas.createSequentialGroup()
                        .addGroup(layout_panel_metricas.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout_panel_metricas.createSequentialGroup()
                                .addComponent(label_value)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(combo_valor_normalizacao, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout_panel_metricas.createSequentialGroup()
                                .addGroup(layout_panel_metricas.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(nom, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pubm, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(prim, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(puba, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pria, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nomh, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 2, Short.MAX_VALUE)))
                        .addGap(10, 10, 10))
                    .addGroup(layout_panel_metricas.createSequentialGroup()
                        .addGroup(layout_panel_metricas.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(noah)
                            .addComponent(loc)
                            .addComponent(noa)
                            .addComponent(fin)
                            .addComponent(fout)
                            .addComponent(nomo)
                            .addComponent(noc)
                            .addComponent(tnoc)
                            .addComponent(woc)
                            .addComponent(wmc)
                            .addComponent(cbc)
                            .addComponent(locm)
                            .addComponent(rfc)
                            .addComponent(hnl))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        layout_panel_metricas.setVerticalGroup(
            layout_panel_metricas.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout_panel_metricas.createSequentialGroup()
                .addContainerGap()
                .addComponent(allM)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pubm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nomh)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(noa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(puba)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(noah)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fin)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(fout)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nomo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(noc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tnoc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(woc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(wmc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cbc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(locm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(rfc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(hnl)
                .addGap(12, 12, 12)
                .addGroup(layout_panel_metricas.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(label_value)
                    .addComponent(combo_valor_normalizacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(12,12))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panel_metricas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(executarClusterizacaoButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panel_metricas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(executarClusterizacaoButton))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        java.awt.Dimension dialogSize = getSize();
        setLocation((screenSize.width-dialogSize.width)/2,(screenSize.height-dialogSize.height)/2);
    }

    private void initListeners() {

    	// ------------------------------- Selecao de projetos -------------------------------
        addAllProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAllProjectsActionPerformed(evt);
            }
        });
        
        addProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addProjectsActionPerformed(evt);
            }
        });
        
        removeProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeProjectsActionPerformed(evt);
            }
        });
        
        removeAllProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAllProjectsActionPerformed(evt);
            }
        });
    	
        // ------------------------------- Selecao das metricas -------------------------------
        allM.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                allMetricsItemStateChanged(evt);
            }
        });
        
        loc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                locItemStateChanged(evt);
            }
        });

        nom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nomItemStateChanged(evt);
            }
        });

        pubm.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                pubmItemStateChanged(evt);
            }
        });

        prim.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                primItemStateChanged(evt);
            }
        });
        
        noa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                noaItemStateChanged(evt);
            }
        });

        puba.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                pubaItemStateChanged(evt);
            }
        });

        pria.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                priaItemStateChanged(evt);
            }
        });
        
        noah.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                noahItemStateChanged(evt);
            }
        });

        nomh.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nomhItemStateChanged(evt);
            }
        });
        
        /**/
        
        fin.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                finItemStateChanged(evt);
            }
        });
        
        fout.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                foutItemStateChanged(evt);
            }
        });
        
        nomo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nomoItemStateChanged(evt);
            }
        });
        
        noc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nocItemStateChanged(evt);
            }
        });
        
        tnoc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                tnocItemStateChanged(evt);
            }
        });
        
        woc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                wocItemStateChanged(evt);
            }
        });
        
        wmc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                wmcItemStateChanged(evt);
            }
        });
        
        cbc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cbcItemStateChanged(evt);
            }
        });
        
        locm.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                locmItemStateChanged(evt);
            }
        });
        
        rfc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rfcItemStateChanged(evt);
            }
        });
        
        hnl.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                hnlItemStateChanged(evt);
            }
        });
        
        // ------------------------------- Executar Clusterizacao -------------------------------
        executarClusterizacaoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                executarClusterizacaoActionPerformed(evt);
            }
        });
    }

    @SuppressWarnings("unchecked")
	private void addAllProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	DefaultListModel model = (DefaultListModel) nonSelectedProjects.getModel();
    	File[] addedProjects = new File[model.size()];
    	
    	for (int i = 0; i < addedProjects.length; i++)
			addedProjects[i] = (File) model.get(i);
    	
    	DefaultListModel selectedModel = (DefaultListModel) selectedProjects.getModel();
    	for (File project : addedProjects)
    		selectedModel.addElement(project);
    	
    	model.clear();
    }
    
    @SuppressWarnings("unchecked")
	private void addProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	int[] selection = nonSelectedProjects.getSelectedIndices();
    	
    	DefaultListModel model = (DefaultListModel) nonSelectedProjects.getModel();
    	File[] addedProjects = new File[selection.length];
    	
    	for (int i = 0; i < selection.length; i++)
			addedProjects[i] = (File) model.get(selection[i]);
    	
    	DefaultListModel selectedModel = (DefaultListModel) selectedProjects.getModel();
    	for (File project : addedProjects)
    		selectedModel.addElement(project);
    	
    	for (int i = 0; i < selection.length; i++)
			model.remove(selection[i]);
    }
    
    @SuppressWarnings("unchecked")
	private void removeProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	int[] selection = selectedProjects.getSelectedIndices();
    	
    	DefaultListModel model = (DefaultListModel) selectedProjects.getModel();
    	File[] removedProjects = new File[selection.length];
    	
    	for (int i = 0; i < selection.length; i++)
			removedProjects[i] = (File) model.get(selection[i]);
    	
    	DefaultListModel selectedModel = (DefaultListModel) nonSelectedProjects.getModel();
    	for (File project : removedProjects)
    		selectedModel.addElement(project);
    	
    	for (int i = 0; i < selection.length; i++)
			model.remove(selection[i]);
    }
    
    @SuppressWarnings("unchecked")
	private void removeAllProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	DefaultListModel model = (DefaultListModel) selectedProjects.getModel();
    	File[] removedProjects = new File[model.size()];
    	
    	for (int i = 0; i < removedProjects.length; i++)
			removedProjects[i] = (File) model.get(i);
    	
    	DefaultListModel selectedModel = (DefaultListModel) nonSelectedProjects.getModel();
    	for (File project : removedProjects)
    		selectedModel.addElement(project);
    	
    	model.clear();
    }
    
    private void allMetricsItemStateChanged(java.awt.event.ItemEvent evt) {
    	
        loc.setSelected(allM.isSelected());
        nom.setSelected(allM.isSelected());
        pubm.setSelected(allM.isSelected());
        prim.setSelected(allM.isSelected());
        nomh.setSelected(allM.isSelected());
        noa.setSelected(allM.isSelected());
        puba.setSelected(allM.isSelected());
        pria.setSelected(allM.isSelected());
        noah.setSelected(allM.isSelected());
        
        fin.setSelected(allM.isSelected());
        fout.setSelected(allM.isSelected());
        nomo.setSelected(allM.isSelected());
        noc.setSelected(allM.isSelected());
        tnoc.setSelected(allM.isSelected());
        woc.setSelected(allM.isSelected());
        wmc.setSelected(allM.isSelected());
        cbc.setSelected(allM.isSelected());
        locm.setSelected(allM.isSelected());
        rfc.setSelected(allM.isSelected());
        hnl.setSelected(allM.isSelected());
        
        updateCheckBox();
    }

    private void locItemStateChanged(java.awt.event.ItemEvent evt) {
        if (loc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.LOC);
        } else {
            this.selectedMetrics.remove(ClusteringController.LOC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void nomItemStateChanged(java.awt.event.ItemEvent evt) {
        if (nom.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOM);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOM);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void pubmItemStateChanged(java.awt.event.ItemEvent evt) {
        if (pubm.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PUBM);
        } else {
            this.selectedMetrics.remove(ClusteringController.PUBM);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void primItemStateChanged(java.awt.event.ItemEvent evt) {
        if (prim.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PRIM);
        } else {
            this.selectedMetrics.remove(ClusteringController.PRIM);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void nomhItemStateChanged(java.awt.event.ItemEvent evt) {
        if (nomh.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOMH);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOMH);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void noaItemStateChanged(java.awt.event.ItemEvent evt) {
        if (noa.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOA);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOA);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void pubaItemStateChanged(java.awt.event.ItemEvent evt) {
        if (puba.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PUBA);
        } else {
            this.selectedMetrics.remove(ClusteringController.PUBA);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void priaItemStateChanged(java.awt.event.ItemEvent evt) {
        if (pria.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PRIA);
        } else {
            this.selectedMetrics.remove(ClusteringController.PRIA);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void noahItemStateChanged(java.awt.event.ItemEvent evt) {
        if (noah.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOAH);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOAH);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void finItemStateChanged(java.awt.event.ItemEvent evt) {
        if (fin.isSelected()) {
            this.selectedMetrics.add(ClusteringController.FIN);
        } else {
            this.selectedMetrics.remove(ClusteringController.FIN);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void foutItemStateChanged(java.awt.event.ItemEvent evt) {
        if (fout.isSelected()) {
            this.selectedMetrics.add(ClusteringController.FOUT);
        } else {
            this.selectedMetrics.remove(ClusteringController.FOUT);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void nomoItemStateChanged(java.awt.event.ItemEvent evt) {
        if (nomo.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOMO);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOMO);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void nocItemStateChanged(java.awt.event.ItemEvent evt) {
        if (noc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOC);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void tnocItemStateChanged(java.awt.event.ItemEvent evt) {
        if (tnoc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.TNOC);
        } else {
            this.selectedMetrics.remove(ClusteringController.TNOC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void wocItemStateChanged(java.awt.event.ItemEvent evt) {
         if (woc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.WOC);
        } else {
            this.selectedMetrics.remove(ClusteringController.WOC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void wmcItemStateChanged(java.awt.event.ItemEvent evt) {
        if (wmc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.WMC);
        } else {
            this.selectedMetrics.remove(ClusteringController.WMC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void rfcItemStateChanged(java.awt.event.ItemEvent evt) {
        if (rfc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.RFC);
        } else {
            this.selectedMetrics.remove(ClusteringController.RFC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void cbcItemStateChanged(java.awt.event.ItemEvent evt) {
        if (cbc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.CBC);
        } else {
            this.selectedMetrics.remove(ClusteringController.CBC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void locmItemStateChanged(java.awt.event.ItemEvent evt) {
        if (locm.isSelected()) {
            this.selectedMetrics.add(ClusteringController.LOCM);
        } else {
            this.selectedMetrics.remove(ClusteringController.LOCM);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    private void hnlItemStateChanged(java.awt.event.ItemEvent evt) {
        if (hnl.isSelected()) {
            this.selectedMetrics.add(ClusteringController.HNL);
        } else {
            this.selectedMetrics.remove(ClusteringController.HNL);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    @SuppressWarnings("unchecked")
	private void executarClusterizacaoActionPerformed(java.awt.event.ActionEvent evt) {
    	StringBuilder errorMessage = new StringBuilder();
    	
    	valorNormalizacao = combo_valor_normalizacao.getSelectedIndex() + 1;
    	if (valorNormalizacao == 0)
    		errorMessage.append("* Select Qmin value\n");
    	
    	if (selectedProjects.getModel().getSize() < 5)
            errorMessage.append("* Select at least five systems\n");
    	
        if (selectedMetrics.isEmpty())
            errorMessage.append("* Select at least one metric\n");
        
        if (errorMessage.length() != 0) {
            JOptionPane.showMessageDialog(this, errorMessage.toString());
        } else {
        	
        	File primaryFolder = new File(clusteringPrimaryFolder);
        	if (!primaryFolder.exists()) primaryFolder.mkdirs();
        	
        	File secondaryFolder = new File(clusteringSecondaryFolder);
        	if (!secondaryFolder.exists()) secondaryFolder.mkdirs();
        	
        	File matricesFolder = new File(clusteringMatricesFolder);
        	if (!matricesFolder.exists()) matricesFolder.mkdirs();
        	
        	try {
        		
        		DefaultListModel model = (DefaultListModel) selectedProjects.getModel();
        		File[] projects = new File[model.size()];
        		for (int i = 0; i < model.size(); i++)
					projects[i] = (File) model.getElementAt(i);
        		
                this.controller = new ClusteringController(projects, primaryFolder, secondaryFolder, matricesFolder, selectedMetrics, valorNormalizacao);
                
                executarClusterizacaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/wait.gif")));
                executarClusterizacaoButton.setText("Executing");
                executarClusterizacaoButton.setEnabled(false);
                
                thread = new Thread(this.controller);
                thread.start();
                
                new Thread(new PanelUpdator()).start();
                
                this.repaint();
	        } catch (Exception e) {
	        	JOptionPane.showMessageDialog(this, e.getLocalizedMessage(), "Error", JOptionPane.ERROR_MESSAGE);
	        }
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private void updateCheckBox() {
    	int selectedMetrics = this.selectedMetrics.size();
    	Integer[] options = new Integer[selectedMetrics];
    	
    	for (int i = 0; i < options.length; i++)
			options[i] = i + 1;
    	
    	combo_valor_normalizacao.setModel(new javax.swing.DefaultComboBoxModel(options));
    	combo_valor_normalizacao.setEnabled(selectedMetrics > 0);
    	if (selectedMetrics > 0) 
    		combo_valor_normalizacao.setSelectedIndex(0);
    }
    
    @SuppressWarnings("unchecked")
	private void retrieveProjects() {
    	File metricsFolder = new File(this.clusteringInputFolder);
    	File[] projects = metricsFolder.listFiles(getMetricFileFilter());
    	
    	boolean existProjects = !metricsFolder.exists() || projects.length == 0;
    	habilitarCampos(!existProjects);
    	
    	if (existProjects) {
    		JOptionPane.showMessageDialog(this, "There is no CSV file in \"metricas\" in the workspace folder." +
    				" Please execute the metrics extraction for your systems.", "Error", JOptionPane.ERROR_MESSAGE);
    	} else {
    		DefaultListModel model = (DefaultListModel) this.nonSelectedProjects.getModel();
    		for (File project : projects) 
    			model.addElement(project);
    	}
    }
    
    private static FileFilter getMetricFileFilter() {
    	return new FileFilter() {
			@Override
			public boolean accept(File pathname) {
			    if (pathname.getName().endsWith(".csv"))
			        return true;
			    return false;
			}
		};
    }
    
    private void habilitarCampos(boolean enable) {
        nonSelectedProjects.setEnabled(enable);
        selectedProjects.setEnabled(enable);
        
        addAllProjectsButton.setEnabled(enable);
        addProjectButton.setEnabled(enable);
        removeProjectButton.setEnabled(enable);
        removeAllProjectsButton.setEnabled(enable);
        
        executarClusterizacaoButton.setEnabled(enable);
        
        allM.setEnabled(enable);
        loc.setEnabled(enable);
        nom.setEnabled(enable);
        pubm.setEnabled(enable);
        prim.setEnabled(enable);
        noa.setEnabled(enable);
        puba.setEnabled(enable);
        pria.setEnabled(enable);
        noah.setEnabled(enable);
        nomh.setEnabled(enable);

        fin.setEnabled(enable);
        fout.setEnabled(enable);
        nomo.setEnabled(enable);
        noc.setEnabled(enable);
        tnoc.setEnabled(enable);
        woc.setEnabled(enable);
        wmc.setEnabled(enable);
        cbc.setEnabled(enable);
        locm.setEnabled(enable);
        rfc.setEnabled(enable);
        hnl.setEnabled(enable);
    }

    private class PanelUpdator implements Runnable {
        @Override
        public void run() {

            while (thread != null && thread.isAlive()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClusteringView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            executarClusterizacaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/start.png")));
            executarClusterizacaoButton.setText("Start");
            executarClusterizacaoButton.setEnabled(true);
            
            JOptionPane.showMessageDialog(ClusteringView.this, "Clustering executed successfully.", "", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private javax.swing.JButton executarClusterizacaoButton;
    
    @SuppressWarnings("rawtypes")
    private javax.swing.JList nonSelectedProjects;
    
    @SuppressWarnings("rawtypes")
    private javax.swing.JList selectedProjects;
    
    private javax.swing.JButton addAllProjectsButton;
    private javax.swing.JButton addProjectButton;
    private javax.swing.JButton removeProjectButton;
    private javax.swing.JButton removeAllProjectsButton;
	
    private javax.swing.JComboBox combo_valor_normalizacao;
    
	private javax.swing.JLabel label_value;
    
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel panel_metricas;
    
    private javax.swing.JScrollPane projetos_nao_selecionados;
    private javax.swing.JScrollPane projetos_selecionados;
    
    private javax.swing.JScrollPane lista_de_metricas;
     
    // Metricas de tamanho iniciais
    private javax.swing.JCheckBox loc;
    private javax.swing.JCheckBox allM;
    private javax.swing.JCheckBox noa;
    private javax.swing.JCheckBox noah;
    private javax.swing.JCheckBox nom;
    private javax.swing.JCheckBox nomh;
    private javax.swing.JCheckBox pria;
    private javax.swing.JCheckBox prim;
    private javax.swing.JCheckBox puba;
    private javax.swing.JCheckBox pubm;
    
    // Demais Metricas
    private javax.swing.JCheckBox fin;
    private javax.swing.JCheckBox fout;
    private javax.swing.JCheckBox nomo;
    private javax.swing.JCheckBox noc;
    private javax.swing.JCheckBox tnoc;
    private javax.swing.JCheckBox woc;
    private javax.swing.JCheckBox wmc;
    private javax.swing.JCheckBox cbc;
    private javax.swing.JCheckBox locm;
    private javax.swing.JCheckBox rfc;
    private javax.swing.JCheckBox hnl;
    
}