package control;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;

/**
 *
 * @author paloma
 * Classe para gerar um vetor de bits, a c�lula fica setada caso a m�trica esteja na aresta
 * inicialmente para m�tricas de tamanho
 */
public class ArestaAnalysisController implements Comparable<ArestaAnalysisController>{

	private String system1,system2;
	private BitSet vetorBit;
	private ArrayList<String> m;
	
		
	public ArestaAnalysisController(Object[] similarMetric) {
		m = new ArrayList<String>();
       //instancia o veotrBit com a quantidade de m�tricas
		for (int i=0;i<similarMetric.length;i++){
			m.add(similarMetric[i].toString());			
		}
		//instancia o vetor com a quantidades de m�tricas (20)
		vetorBit = new BitSet(20);
	}
	
	//para comparar a cardinalidade de duas arestas e colocar em ordem descendente
	public int compareTo(ArestaAnalysisController outraAresta) {
        if (this.getVetorBit().cardinality() < outraAresta.getVetorBit().cardinality()) {
            return 1;
        }
        if (this.getVetorBit().cardinality() > outraAresta.getVetorBit().cardinality()) {
            return -1;
        }
        return 0;
    }
	
	//verifica quais as m�tricas est�o "ligadas" em uma determinada aresta
	public ArestaAnalysisController returnData(String s1, String s2){
		this.setSystem1(s1);
		this.setSystem2(s2);
		
		if(m.contains("numberOfLinesOfCode")){
				vetorBit.set(0);
		}
		if(m.contains("numberOfMethods")){
				vetorBit.set(1);
		}
		if(m.contains("numberOfPublicMethods")){
				vetorBit.set(2);
		}
		if(m.contains("numberOfPrivateMethods")){
			vetorBit.set(3);
	    } 
		if(m.contains("numberOfMethodsInherited")){
			vetorBit.set(4);
	}
		
		if(m.contains("numberOfAttributes")){
			vetorBit.set(5);
	}
		if(m.contains("numberOfPublicAttributes")){
			vetorBit.set(6);
	}	
		if(m.contains("numberOfPrivateAttributes")){
			vetorBit.set(7);
	}	
		if(m.contains("numberOfAttributesInherited")){
			vetorBit.set(8);
	}
		if(m.contains("fanIn")){
			vetorBit.set(9);
	}	
		if(m.contains("fanOut")){
			vetorBit.set(10);
	}	
		if(m.contains("numberOfMethodsOverriden")){
			vetorBit.set(11);
	}	
		if(m.contains("numberOfChildren")){
			vetorBit.set(12);
	}	
		if(m.contains("totalNumberOfChildren")){
			vetorBit.set(13);
	}	
		if(m.contains("weightOfAClass")){
			vetorBit.set(14);
	}	
		if(m.contains("weightedMethodCount")){
			vetorBit.set(15);
	}	
		if(m.contains("couplingBetweenClasses")){
			vetorBit.set(16);
	}	
		if(m.contains("lackOfCohesionInMethods")){
			vetorBit.set(17);
	}	
		if(m.contains("responseForClass")){
			vetorBit.set(18);
	}	
		if(m.contains("hierarchyNestingLevel")){
			vetorBit.set(19);
	}	
		
		
		
		
		/*
		 * 	

		 */
			
		System.out.println("Vetor de BITS "+system1+"-"+system2+": "+vetorBit);	
		setVetorBit(vetorBit);		
		return this;
	}
	
	public String getSystem1() {
		return system1;
	}

	public void setSystem1(String system1) {
		this.system1 = system1;
	}

	public String getSystem2() {
		return system2;
	}

	public void setSystem2(String system2) {
		this.system2 = system2;
	}

	public BitSet getVetorBit() {
		return vetorBit;
	}

	public void setVetorBit(BitSet vetorBit) {
		this.vetorBit = vetorBit;
	}

	public ArrayList<String> getM() {
		return m;
	}

	public void setM(ArrayList<String> m) {
		this.m = m;
	}

}
