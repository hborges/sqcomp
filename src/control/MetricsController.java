/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author hudson
 */
public class MetricsController implements Runnable {

    private File pathMoose;
    private File[] fontes;
    private File saida;
    private int numProjetos;
    private int analisados;
    private List<File> erros;
    private StringBuilder output;

    public MetricsController(File saida, File[] fontes) {
    	this.output = new StringBuilder();
        this.pathMoose = new File("Moose.app");
        this.saida = saida;
        this.fontes = fontes;
        this.erros = new ArrayList<File>();
    }

    @Override
    public void run() {

        numProjetos = fontes.length;
        analisados = 0;

        Process process = null;
        String comando = null;
        String erro = null;

        if (this.pathMoose.exists()) {

            if (System.getProperty("os.name").toLowerCase().contains("linux")) {
                this.pathMoose = new File(pathMoose.getAbsoluteFile() + File.separator + "Contents" + File.separator + "Linux");
            } else if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                this.pathMoose = new File(pathMoose.getAbsoluteFile() + File.separator + "Contents" + File.separator + "Windows");
            }

            for (File fonte : fontes) {

                try {
                	output.append("Extracting metrics from: ").append(fonte.getName()).append("\n");
                    if (System.getProperty("os.name").toLowerCase().contains("linux")) {
                        comando = "./squeak ./../Resources/Moose.image runMetrics.st " + fonte.getAbsolutePath() + " " + saida.getAbsolutePath() + File.separator + fonte.getName().replace(".mse", ".csv");
                    } else if (System.getProperty("os.name").toLowerCase().contains("windows")) {
                        comando = pathMoose.getAbsolutePath()+File.separator+"Squeak.exe runMetrics.st " + fonte.getAbsolutePath() + " " + saida.getAbsolutePath() + File.separator + fonte.getName().replace(".mse", ".csv");
                    }
                    process = Runtime.getRuntime().exec(comando, null, pathMoose);
                    process.waitFor();
                    
                    erro = new BufferedReader(new InputStreamReader(process.getInputStream())).readLine();
                    if (erro != null && !erro.isEmpty()) {
                    	output.append("Problem occurred in the extraction:\n");
                        output.append(erro).append("\n");
                        
                        this.erros.add(fonte);
                    } else
                    	output.append("Metric Extraction finished successfully.\n");
                    
                    erro = null;
                    
                    output.append("=========================================== \n");
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
                }
                analisados++;
            }

        }

    }
    
    public String getOutput() {
    	return output.toString();
    }

    public int getAnalisados() {
        return analisados;
    }

    public List<File> getErros() {
        return erros;
    }

    public int getNumProjetos() {
        return numProjetos;
    }
}
