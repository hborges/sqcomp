package control;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collection;
import java.util.Collections;
import java.util.Set;

import javax.swing.DefaultListModel;

import control.cluster.ClusteringInfo;


/**
 *
 * @author paloma
 * Analisa todas as cliques e considera apenas as cliques nas quais as arestas possuem as mesmas m�tricas de acordo
 * com o limiar. As cliques que devem ser descartadas s�o setadas como null.
 */

public class ClickAnalysisController {
	
	private Object[] allCliques;
	private ClusteringInfo clusteringInfo;
	
	private int limiar;
	
	public ClickAnalysisController(Object[] allMaximalCliques, int limiar,ClusteringInfo clusteringInfo) {
		allCliques = allMaximalCliques;
		this.limiar = limiar;
		this.clusteringInfo = clusteringInfo;		
	}
	
	public Object[] analisaCLick() {
		System.out.println(" ");
		System.out.println("Quantidade de cliques: "+allCliques.length);
		
		//analisa todas as cliques
		for (int i=0;i < allCliques.length;i++){
		  	System.out.println("Clique Analisada: "+allCliques[i].toString());
		  	ArrayList<ArestaAnalysisController> datas = new ArrayList<ArestaAnalysisController>();
		  	//transforma o vetor de object[] em arrayList para facilitar a manipula��o da clique
		  	ArrayList al = new ArrayList((Collection)allCliques[i]);
		  	//se clique menor ou igual a 2 sempre terao as mesmas metricas, nao faz nada!
		  	if (al.size() > 2){
		  		String system1, system2;
		  		System.out.println("Tamanho da clique: "+al.size());
		  		//analiso se a clique possui as mesmas m�tricas de acordo com o limiar
		  		for (int j=0;j < al.size();j++){
		  			system1 = al.get(j).toString();		  			
		  			for (int aux=j+1;aux < al.size();aux++){
		  				system2 = al.get(aux).toString();		  			
		            	Object[] similarMetrics = clusteringInfo.getSimilarMetrics(system1, system2);
		            	//armazena objeto com as informa��es da aresta da clique e o vetor de bits informando as m�tricas da aresta
		            	datas.add(new ArestaAnalysisController(similarMetrics).returnData(system1, system2)); 
		  			}		  			
		  		}
		  		System.out.println("");
		  		//faz a ordena��o do array datas considerando a cardinalidade da maior aresta, ou seja, as arestas com um maior n�mero de m�tricas estara em primeiro lugar
		  		Collections.sort(datas);
		  		
		  		/*//verificar se foi ordenado corretamente - apenas para teste- apagar
		  		  for (ArestaAnalysisController obj : datas) {  
		  		    System.out.println (obj.getSystem1() + "," + obj.getSystem2()+" arestas em comum: "+obj.getVetorBit());  
		  		} */ 
		  		
		  		//faz intersecao das arestas
		  		if (intersecaoArestas(datas,limiar)){
		  			//existe clique
		  			System.out.println("");
		  			System.out.println("Clique Considerada");
		  			System.out.println("");
		  		}else{
		  			//clique invalida
		  			System.out.println("");
		  			System.out.println("Clique descartada...");
		  			System.out.println("");
		  			
		  			allCliques[i] = null;		  			
		  		}
		  	}//fim if		  	
		}
		//verifica resultado no console - teste - depois APAGAR
		System.out.println("REsultado vetor final");
		for(int i=0;i<allCliques.length;i++){
			System.out.println("i= "+i+" - "+allCliques[i]);
		}
		return allCliques;
	}

	//analisa as arestas de uma determinada clique, buscando as interse��es
	private boolean intersecaoArestas(ArrayList<ArestaAnalysisController> datas2, int limiar) {
		//o vetor dat armazena todas as arestas da clique, no campo VetorBit estao as m�tricas de cada aresta
		ArrayList<ArestaAnalysisController> dat = datas2;
		//quantidade de arestas na clique analisada
		System.out.println("dat.size():"+dat.size());
		boolean teste = false;
		//pego a primeira aresta (aresta com maior quantidade de m�tricas)
		BitSet aux = (BitSet) dat.get(0).getVetorBit().clone();
		for (int i=1;i<dat.size();i++){
			System.out.println(dat.get(0).getSystem1()+"-"+dat.get(0).getSystem2()+" aux inicial:"+aux);
			BitSet aux2 = (BitSet) dat.get(i).getVetorBit().clone();
			//realiza a interse�ao entre duas arestas e armazena o resultado em aux
			aux.and(aux2);
		    System.out.println(dat.get(i).getSystem1()+"-"+dat.get(i).getSystem2()+" intersecao com "+ dat.get(0).getSystem1()+"-"+dat.get(0).getSystem2()+ aux);
			System.out.println("limiar: "+limiar);
		    //veriica a cardinalidade da interse�ao
		    if (aux.cardinality() >= limiar){
				   //ok, continuo a analisar a clique
				   teste=true;				   
			   }else{
				   //descarta a clique e sai do for
				   teste = false;
				   System.out.println();
				   System.out.println("SAII do for");
				   System.out.println();
				   break;			   
			   }
		}
		
		if (teste){
			return teste;
		}else{ 
			return teste;}
	}

	private boolean intersecaoArestas2(ArrayList<ArestaAnalysisController> datas2, int limiar) {
		//o vetor dat armazena todas as arestas da clique, no campo VetorBit estao as m�tricas de cada aresta
		ArrayList<ArestaAnalysisController> dat = datas2;
		boolean teste = false;
		//quantidade de arestas na clique analisada
		System.out.println("dat.size():"+dat.size());
		for (int i=0;i<dat.size();i++){
			BitSet aux = (BitSet) dat.get(i).getVetorBit().clone();			
			for (int j=i+1;j<dat.size();j++){
				System.out.println(dat.get(i).getSystem1()+"-"+dat.get(i).getSystem2()+" aux inicial:"+aux);
				BitSet aux2 = (BitSet) dat.get(j).getVetorBit().clone();
				System.out.println(dat.get(j).getSystem1()+"-"+dat.get(j).getSystem2()+" aux2 inicial:"+aux2);
		     	aux.and(aux2);
		
			   System.out.println(dat.get(i).getSystem1()+"-"+dat.get(i).getSystem2()+" intersecao com "+ dat.get(j).getSystem1()+"-"+dat.get(j).getSystem2()+ aux);
			   if (aux.cardinality()>=limiar){
				   //ok, continuo a analisar a clique
				   teste=true;				   
			   }else{
				   //descarta a clique e sai do for
				   teste = false;
				  // j = datas2.size()-1;
				   //i = datas2.size()-1;
				   System.out.println("SAIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII");
				  
				   break;
				   //aux = (BitSet) datas2.get(i).getVetorBit().clone();
			   }
		}
		}
		if (teste){
			return teste;
		}else{ 
			return teste;}
	}

}
