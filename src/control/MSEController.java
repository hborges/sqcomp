/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import fr.inria.verveine.extractor.java.VerveineJParser;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author hudson
 */
public class MSEController implements Runnable {

    private File pastaSaida;
    private File[] systems;
    private StringBuilder output;
    private int numProjetos;
    private int analisados;
    private List<File> erros;
    private VerveineJParser parser;

    public MSEController(String pastaSaida, File[] projects) {
        this.output = new StringBuilder();
        this.erros = new ArrayList<File>();
        this.pastaSaida = new File(pastaSaida != null ? pastaSaida : "");
        this.systems = projects;
        this.numProjetos = projects.length;
        this.analisados = 0;
    }

    public String getOutput() {
        return this.output.toString();
    }

    public int getAnalisados() {
        return analisados;
    }

    public int getNumProjetos() {
        return numProjetos;
    }

    public List<File> getErros() {
        return erros;
    }

    @Override
    public void run() {

        File log;
        PrintWriter printWriter;

        if (systems != null && pastaSaida != null) {

            try {

                while (analisados < numProjetos) {

                    parser = new VerveineJParser();
                    parser.setOptions(new String[]{systems[analisados].getAbsolutePath()});
                    output.append("Extracting MSE from: ").append(systems[analisados].getName()).append("\n");
                    try {
                        output.append("Starting static analysis ... \n");
                        parser.parse();
                        output.append("Saving analysis ... \n");
                        parser.emitMSE(pastaSaida.getAbsolutePath() + File.separator + systems[analisados].getName() + ".mse");
                        output.append("Data analysis completed successfully ... \n");
                        output.append("=========================================== \n");
                    } catch (Error e) {
                        output.append("Problem occurred during extraction:\n");
                        for (StackTraceElement et : e.getStackTrace()) {
                            output.append(et.toString()).append("\n");
                        }
                        if (!erros.contains(systems[analisados])) {
                            erros.add(systems[analisados]);
                        }
                        output.append("=========================================== \n");
                        log = new File(pastaSaida.getAbsoluteFile() + File.separator + systems[analisados].getName() + ".log");
                        log.createNewFile();
                        printWriter = new PrintWriter(log);
                        e.printStackTrace(printWriter);
                        printWriter.close();
                    }

                    parser = null;
                    System.gc();
                    analisados++;
                }

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
            }

        } else {
            output.append("Invalid Parameters");
        }

    }

	public String getInput() {
		// TODO Auto-generated method stub
		return null;
	}
}
