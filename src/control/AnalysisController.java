/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.geom.Rectangle2D;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.jgraph.JGraph;
import org.jgraph.graph.AttributeMap;
import org.jgraph.graph.DefaultEdge;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.DirectedGraph;
import org.jgrapht.alg.BronKerboschCliqueFinder;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DefaultDirectedGraph;

import util.graph.SimilarityEdge;
import control.algorithms.BronKerboschAlgoritm;
import control.cluster.ClusteringInfo;
import data.FileController;
import data.structure.UndirectedGraph;
import data.structure.exceptions.EdgeException;
import data.structure.exceptions.VertexException;

/**
 *
 * @author hudson, gustavojss
 */
public class AnalysisController {

    @SuppressWarnings("rawtypes")
	private JGraphModelAdapter adapter;
    private JGraph jGraph;
    private DirectedGraph<String, DefaultEdge> g;
    
    private List<String> vertices;
    private List<String[]> arestas;
    private ClusteringInfo clusteringInfo;
    
    private UndirectedGraph<String> my_graph = new UndirectedGraph<String>();
    
    public ClusteringInfo getClusteringInfo() {
		return clusteringInfo;
	}

	public void setClusteringInfo(ClusteringInfo clusteringInfo) {
		this.clusteringInfo = clusteringInfo;
	}

	
    
    @SuppressWarnings("unchecked")
	public AnalysisController(File arquivo) throws FileNotFoundException, IOException, VertexException, EdgeException {
    	Object[] graphPresentation = FileController.getArestasVerticesFromCSV(arquivo);
    	
    	setClusteringInfo(clusteringInfo = new ClusteringInfo(arquivo));
    	
        this.vertices = (List<String>) graphPresentation[0];
        for(String vertice : this.vertices){
        	my_graph.addVertex(vertice, vertice);
        }
        
        this.arestas = (List<String[]>) graphPresentation[1];
        for(String[] arestas : this.arestas){
        	my_graph.addEdge(arestas[0],arestas[1]);
        }
        
    }
    
    public AnalysisController(List<String> vertices2, List<String[]> arestas2) {
		// TODO Auto-generated constructor stub
	}

	public JGraph getjGraph() {
        return jGraph;
    }
    
	public JGraph getDirectedGraph(Dimension dimension) throws Exception {
    	return this.createGraph(vertices, arestas, dimension);
    }

    public JGraph destacar(List<String> vertices, Dimension dimension) throws Exception {
    	return this.createGraph(vertices, arestas, dimension);
    }
    
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private JGraph createGraph(List<String> vertices, List<String[]> arestas, Dimension dimension) throws Exception {
    	
        g = new DefaultDirectedGraph<String, DefaultEdge>(DefaultEdge.class);

        // Adicionando v�rtices e arestas
        for (String v : vertices) g.addVertex(v);

        for (String[] a : arestas) {
        	String source = a[0];
        	String target = a[1];
        	
            if (a.length == 2 
            		&& vertices.contains(source) && vertices.contains(target) 
            		&& !source.equals(target)) {
            	
            	int numSimilarMetrics = clusteringInfo.getNumSimilarMetrics(source, target);
            	Object[] similarMetrics = clusteringInfo.getSimilarMetrics(source, target);
            	
            	g.addEdge(source, target, new SimilarityEdge<String>(source, target, numSimilarMetrics, similarMetrics));
            } else if (a.length != 2) {
            	throw new IllegalArgumentException("Invalid Parameters");
            }
        }
        
        adapter = new JGraphModelAdapter(g);
        
        // Distribuindo os v�rtices pelo espa�o circular
        final float angulo = new Float(360) / vertices.size();
        int raio = (int) ((dimension.getHeight() > dimension.getWidth()) ? 
        		dimension.getWidth() / 2 : 
        		dimension.getHeight() / 2);
        
        float cont = 0;
        for (String v : vertices) {
            Point p = getPosition(raio, cont);
            this.positionVertexAt(v, (int) p.getX(), (int) p.getY());
            cont += angulo;
        }

        // Criando o grafo
        jGraph = new JGraph(adapter);
        jGraph.setPreferredSize(dimension);
        
        return jGraph;
    }
    
    private static Point getPosition(int raio, float grau) {
        double x = Math.cos(Math.toRadians(grau)) * raio + raio;
        double y = Math.sin(Math.toRadians(grau)) * (raio * 0.95) + (raio * 0.95);
        return new Point((int) x, (int) y);
    }

    @SuppressWarnings("unchecked")
	private void positionVertexAt(Object vertex, int x, int y) {
        DefaultGraphCell cell = adapter.getVertexCell(vertex);
        AttributeMap attr = cell.getAttributes();
        Rectangle2D bounds = GraphConstants.getBounds(attr);

        Rectangle2D newBounds =
                new Rectangle2D.Double(x, y, 
                bounds.getWidth(),
                bounds.getHeight());

        GraphConstants.setBounds(attr, newBounds);

        AttributeMap cellAttr = new AttributeMap();
        cellAttr.put(cell, attr);
        adapter.edit(cellAttr, null, null, null);
    }
    
    @SuppressWarnings({ "rawtypes" })
	public Collection getAllMaximalCliques() {
        
    	//BronKerboschCliqueFinder cliqueFinder = new BronKerboschCliqueFinder(g);
    	
    	BronKerboschAlgoritm cliqueFinder = new BronKerboschAlgoritm(my_graph);
    	
    	return cliqueFinder.getAllMaximalCliques();
    	
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public Collection getBiggestMaximalCliques() {
        BronKerboschCliqueFinder cliqueFinder = new BronKerboschCliqueFinder(g);
        return cliqueFinder.getBiggestMaximalCliques();
    }
}