package control.cluster;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import util.Util;

import com.csvreader.CsvReader;
import com.stromberglabs.cluster.Cluster;
import com.stromberglabs.cluster.Clusterable;
import com.stromberglabs.cluster.KClusterer;
import com.stromberglabs.cluster.KMeansClusterer;
import com.stromberglabs.cluster.Point;

import data.FileController;

/**
 *
 * @author hudson, gustavojss
 */
public class ClusteringController implements Runnable {
	
	public static final Integer LOC = 1, NOM = 2, PUBM = 3, PRIM = 4, NOMH = 5, NOA = 6, PUBA = 7, PRIA = 8, NOAH = 9;
	public static final Integer FIN = 10, FOUT = 11, NOMO = 12, NOC = 13, TNOC = 14, WOC = 15, WMC = 16, CBC = 17, LOCM = 18, RFC = 19, HNL = 20;
	
	private static final Integer NUM_GROUPS = 5;
	private static final char SEPARATOR = ',';
	
	private static final Integer[] ALL_METRICS = {LOC, NOM, PUBM, PRIM, NOMH, NOA, PUBA, PRIA, NOAH,
		 										  FIN,FOUT,NOMO,NOC,TNOC,WOC,WMC,CBC,LOCM,RFC,HNL};
	
	private static final String[] METRIC_NAMES = {"Loc", "Nom", "PubM", "PriM", "NomH", "Noa", "PubA", "PriA", "NoaH",
												  "Fin","Fout","NomO","NoC","TnoC","WoC","WmC","CbC","LocM","RfC","HnL"};
	
	private static final String[] METRIC_QUALIFIED_NAMES = {"numberOfLinesOfCode",
		 													"numberOfMethods",
		 													"numberOfPublicMethods",
		 													"numberOfPrivateMethods",
		 													"numberOfMethodsInherited",
		 													"numberOfAttributes",
		 													"numberOfPublicAttributes",
		 													"numberOfPrivateAttributes",
		 													"numberOfAttributesInherited",
		 													"fanIn",
		 													"fanOut",
		 													"numberOfMethodsOverriden",
		 													"numberOfChildren",
		 													"totalNumberOfChildren",
		 													"weightOfAClass",
		 													"weightedMethodCount",
		 													"couplingBetweenClasses",
		 													"lackOfCohesionInMethods",
		 													"responseForClass",
		 													"hierarchyNestingLevel"};
	
	private Map<Integer, Map<String, Cluster>> primaryClusters;
	private Map<Integer, Map<String, Cluster>> secondaryClusters;
	private Object[][] groups;
	private Object[][] matrizNaoDiscretizada;
	private Object[][] matrizDiscretizada;
	
	private File[] sources;
	private File primaryClusterOutput, secondaryClusterOutput, matricesOutput;
	private Set<Integer> options;
	private Integer threshold;
	
	private String clusteringDate;
	
    public ClusteringController(File[] sources, File primaryClusterOutput, File secondaryClusterOutput, 
    		File matricesOutput, Set<Integer> opts, int valor) {
    	
        this.sources = sources;
        this.primaryClusterOutput = primaryClusterOutput;
        this.secondaryClusterOutput = secondaryClusterOutput;
        this.matricesOutput = matricesOutput;
        
        this.options = opts;
        this.threshold = valor;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
	private void primaryCluster() {

        CsvReader reader = null;
        KClusterer kmc = new KMeansClusterer();
        Map<Integer, List<Clusterable>> clustersPerMetric;

        String[] headers, values;
        
        float[] groups;
        Cluster[] primaryCluster;

        // Criando clusters prim�rios
        primaryClusters = new HashMap<Integer, Map<String,Cluster>>();
        for (Integer metric : ALL_METRICS) {
        	primaryClusters.put(metric, new TreeMap<String, Cluster>());
        }
        
        for (File file : sources) {
            try {
                reader = new CsvReader(file.getAbsolutePath());
                reader.setDelimiter(SEPARATOR);
                reader.setTrimWhitespace(true);

                // Recuperando os cabe�alhos do arquivo .csv
                if (reader.readHeaders()) {
                    headers = reader.getHeaders();
                } else {
                    continue;
                }

                // Recuperando o �ndice das m�tricas no arquivo .csv
                Map<Integer, Integer> metricsIndex = new HashMap<Integer, Integer>();
                for (Integer metric : ALL_METRICS) {
                	metricsIndex.put(metric, Util.linearSearch(headers, METRIC_QUALIFIED_NAMES[metric-1]));
                }
                
                // Criando listas de clusters por m�trica
                clustersPerMetric = new TreeMap<Integer, List<Clusterable>>();
                for (Integer metric : ALL_METRICS) {
                	clustersPerMetric.put(metric, new LinkedList<Clusterable>());
                }
                
                // Recuperando valores das m�tricas
                while (reader.readRecord()) {
                    values = reader.getValues();
                    try {
                    	for (Integer metric : ALL_METRICS) {
                    		int metricIndex = metricsIndex.get(metric);
                    		if (options.contains(metric) && metricIndex != -1)
                    			clustersPerMetric.get(metric).add(new Point(Float.valueOf(values[metricIndex]), 0));
                    	}
                    } catch (Exception e) {
                    }
                }

                Comparator comparator = getPointComparator();
                
                for (Integer metric : ALL_METRICS) {
                	if (options.contains(metric)) {
                		// Ordenando os valores das m�tricas
                		List<Clusterable> metricValues = clustersPerMetric.get(metric);
                		Collections.sort(metricValues, comparator);
                		
                		// Realizando o agrupamento
                		primaryCluster = kmc.cluster(metricValues, NUM_GROUPS);
                		
                		// Criando os clusters gerados
                		groups = new float[NUM_GROUPS];
                		for (int i = 0; i < primaryCluster.length; i++) {
                			groups[i] = primaryCluster[i].getLocation()[0];
                		}
                		
                		// Guardando os clusters gerados no mapa
                		Map<String, Cluster> metricCluster = primaryClusters.get(metric);
                		Cluster cluster = new Cluster(groups, metricCluster.size());
                		metricCluster.put(file.getName(), cluster);
                	}
                }
            } catch (IOException ex) {
                Logger.getLogger(ClusteringController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        reader.close();

        // Salvando os clusters prim�rios em arquivo
        for (Integer metric : ALL_METRICS) {
        	if (options.contains(metric)) {
        		try {
        			Map<String, Cluster> metricCluster = primaryClusters.get(metric);
        			FileController.savePrimaryOrSecundaryCluster(metricCluster, 
        					primaryClusterOutput.getAbsolutePath().concat(File.separator).concat("clusterPri" + METRIC_NAMES[metric-1] + ".csv"));
        		} catch (IOException e) {
        			Logger.getLogger(ClusteringController.class.getName()).log(Level.SEVERE, null, e);
        		}
        	}
        }
    }
    
    private static Comparator<Point> getPointComparator() {
    	return new Comparator<Point>() {
            @Override
            public int compare(Point t, Point t1) {
                if (t.getX() > t1.getX()) {
                    return 1;
                } else if (t.getX() < t1.getX()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        };
    }

    private void secundaryCluster() {

        KClusterer kmc = new KMeansClusterer();

        // Criando clusters secund�rios
        secondaryClusters = new HashMap<Integer, Map<String,Cluster>>();
        
        for (Integer metric : ALL_METRICS) {
        	if (options.contains(metric)) {
        		Map<String, Cluster> secondaryCluster = new TreeMap<String, Cluster>();
        		
        		// Realizando o agrupamento
        		Map<String, Cluster> primaryCluster = primaryClusters.get(metric);
        		Cluster[] clusters = kmc.cluster(new ArrayList<Clusterable>(primaryCluster.values()), NUM_GROUPS);
        		
        		// Guardando os clusters gerados no mapa
        		for (Cluster cluster : clusters) {
        			secondaryCluster.put("Cluster" + cluster.getId(), cluster);
        		}
        		secondaryClusters.put(metric, secondaryCluster);
        		
        		// Salvando os clusters secund�rios em arquivo
        		try {
        			FileController.savePrimaryOrSecundaryCluster(secondaryCluster, 
        					secondaryClusterOutput.getAbsolutePath().concat(File.separator).concat("clusterSec" + METRIC_NAMES[metric-1] + ".csv"));
        		} catch (IOException ex) {
        			Logger.getLogger(ClusteringController.class.getName()).log(Level.SEVERE, null, ex);
        		}
        	}
        }
    }

    private void groupClusters() {
    	
    	// Construindo tabela de agrupamento
        String[] sistemas = new String[sources.length];
        groups = new Object[sources.length + 1][options.size() + 1];
        groups[0][0] = "Sistema/Metrica";

        // Adicionando o nome dos sistemas
        for (int i = 0; i < sources.length; i++) {
            sistemas[i] = sources[i].getName();
            groups[i + 1][0] = sources[i].getName();
        }

        // Adicionando m�tricas
        int columnCount = 0;
        for (Integer metric : ALL_METRICS) {
        	if (options.contains(metric)) {
        		
        		groups[0][++columnCount] = METRIC_QUALIFIED_NAMES[metric-1];
        		
        		Map<String, Cluster> primaryCluster = primaryClusters.get(metric);
        		Map<String, Cluster> secondaryCluster = secondaryClusters.get(metric);
        		
                for (String s : primaryCluster.keySet()) {
                    for (Cluster c : secondaryCluster.values()) {
                        if (c.getItems().contains(primaryCluster.get(s))) {
                            groups[Util.linearSearch(sistemas, s) + 1][columnCount] = c.getId() + 1;
                            break;
                        }
                    }
                }
        	}
        }
        
        // Salvando a tabela de agrupamento em arquivo
        try {
            FileController.saveMatrix(Arrays.asList(groups), 
            		matricesOutput.getAbsolutePath().concat(File.separator).concat("GroupsSystems" + getDate() + ".csv"));
        } catch (IOException ex) {
            Logger.getLogger(ClusteringController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void systemsAnalisys() {

        matrizNaoDiscretizada = new Object[sources.length + 1][sources.length + 1];
        matrizDiscretizada = new Object[sources.length + 1][sources.length + 1];

        for (int i = 1; i <= sources.length; i++) {
            for (int j = 1; j <= sources.length; j++) {
                matrizNaoDiscretizada[i][j] = 0;
                matrizDiscretizada[i][j] = 0;
            }
        }

        matrizNaoDiscretizada[0][0] = "Sistema/Sistema";
        matrizDiscretizada[0][0] = "Sistema/Sistema";

        String[] sistemas = new String[sources.length];

        for (int i = 1; i <= sources.length; i++) {
            matrizNaoDiscretizada[i][0] = groups[i][0];
            matrizNaoDiscretizada[0][i] = groups[i][0];
            matrizDiscretizada[i][0] = groups[i][0];
            matrizDiscretizada[0][i] = groups[i][0];
            sistemas[i - 1] = groups[i][0].toString();
        }

        for (int i = 1; i <= sources.length; i++) {
            for (int j = 1; j <= options.size(); j++) {
                for (int k = 1; k <= sources.length; k++) {
                    if (groups[i][j] == groups[k][j]) {
                        matrizNaoDiscretizada[i][k] = new Integer(matrizNaoDiscretizada[i][k].toString()) + 1;
                    }
                }
            }
        }

        try {
            FileController.saveMatrix(Arrays.asList(matrizNaoDiscretizada), matricesOutput.getAbsolutePath().concat(File.separator).concat("MatrizNaoDiscretizada" + getDate() + ".csv"));
        } catch (IOException ex) {
            Logger.getLogger(ClusteringController.class.getName()).log(Level.SEVERE, null, ex);
        }

        for (int i = 1; i <= sources.length; i++) {
            for (int j = 1; j <= sources.length; j++) {
                matrizDiscretizada[i][j] = new Integer(matrizNaoDiscretizada[i][j].toString()) >= threshold ? 1 : 0;
            }
        }

        try {
            FileController.saveMatrix(Arrays.asList(matrizDiscretizada), matricesOutput.getAbsolutePath().concat(File.separator).concat("MatrizDiscretizada" + getDate() + ".csv"));
        } catch (IOException ex) {
            Logger.getLogger(ClusteringController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    private String getDate() {
    	if (this.clusteringDate == null) {
	    	this.clusteringDate = "";
	    	Calendar calendar = Calendar.getInstance();
	    	
	    	this.clusteringDate += calendar.get(Calendar.DATE);
	    	this.clusteringDate += "-" + calendar.get(Calendar.MONTH);
	    	this.clusteringDate += "-" + calendar.get(Calendar.YEAR);
	    	this.clusteringDate += " " + calendar.get(Calendar.HOUR);
	    	this.clusteringDate += "-" + calendar.get(Calendar.MINUTE);
	    	this.clusteringDate += "-" + calendar.get(Calendar.SECOND);
    	}
    	
    	return this.clusteringDate;
    }

    @Override
    public void run() {

        this.primaryCluster();
        this.secundaryCluster();
        this.groupClusters();
        this.systemsAnalisys();

    }
}