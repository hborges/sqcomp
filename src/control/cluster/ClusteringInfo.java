package control.cluster;

import com.csvreader.CsvReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import util.Util;

public class ClusteringInfo {
	
	private Object[][] matrizNaoDiscretizada;
	private Object[][] systemGroups;
	private Object[] systemGroupsColumn;

	public ClusteringInfo(File arquivo) throws FileNotFoundException, IOException {
		final String fileName = "Discretizada";
		
		int dateIndex = arquivo.getAbsolutePath().indexOf(fileName);
		int exteIndex = arquivo.getAbsolutePath().indexOf(".csv");
		
		if (dateIndex >= 0) {
			String data = arquivo.getAbsolutePath().substring(dateIndex + fileName.length(), exteIndex);
			
			String parent = arquivo.getParentFile().getAbsolutePath() + File.separator;
			matrizNaoDiscretizada = getMatrix(parent + "MatrizNaoDiscretizada" + data + ".csv");
			systemGroups = getMatrix(parent + "GroupsSystems" + data + ".csv");
			
			systemGroupsColumn = new Object[systemGroups.length];
			for (int i = 0; i < systemGroups.length; i++)
				systemGroupsColumn[i] = systemGroups[i][0];
		} else {
			matrizNaoDiscretizada = new Object[1][1];
			systemGroups = new Object[1][1];
			systemGroupsColumn = new Object[1];
		}
		
	}
	
	private static Object[][] getMatrix(String arquivo) throws FileNotFoundException, IOException {
		
        CsvReader reader = new CsvReader(arquivo);
        reader.setDelimiter(';');
        reader.setTrimWhitespace(true);

        List<String[]> matrixList = new ArrayList<String[]>();
        
        reader.readHeaders();
        String[] headers = reader.getHeaders();
        for (int i = 1; i < headers.length; i++)
        	if (headers[i].contains("."))
        		headers[i] = headers[i].substring(0, headers[i].indexOf('.'));
        matrixList.add(headers);
        
        while (reader.readRecord()) {
        	String[] record = reader.getValues();
        	record[0] = record[0].substring(0, record[0].indexOf('.'));
        	matrixList.add(record);
        }
        
        Object[][] matrix = new Object[matrixList.size()][headers.length];
        int cont = 0;
        for (String[] row : matrixList) {
        	matrix[cont] = row;
        	cont++;
        }
        
		return matrix;
	}
	
    public int getNumSimilarMetrics(String system1, String system2) {
    	
    	Object[] headers = matrizNaoDiscretizada[0];
    	int index1 = Util.linearSearch(headers, system1);
    	int index2 = Util.linearSearch(headers, system2);
    	
    	if (index1 != -1 && index2 != -1)
    		return Integer.parseInt(matrizNaoDiscretizada[index1][index2].toString());
    	
    	return 0;
    }
    
    public Object[] getSimilarMetrics(String system1, String system2) {
    	
    	List<String> similarMetrics = new LinkedList<String>();
    	
    	int index1 = Util.linearSearch(systemGroupsColumn, system1);
    	int index2 = Util.linearSearch(systemGroupsColumn, system2);
    	
    	// para todas as m�tricas, verificar os grupos os quais os sistemas pertencem
    	// caso os grupos forem os mesmos, adicionar a m�trica ao grupo de m�tricas similares
    	for (int i = 1; i < systemGroups[0].length; i++) {
			String group1 = systemGroups[index1][i].toString();
			String group2 = systemGroups[index2][i].toString();
			
			if (group1.equals(group2)) 
				similarMetrics.add(systemGroups[0][i].toString());
		}
    		
    	return similarMetrics.toArray();
    }
}