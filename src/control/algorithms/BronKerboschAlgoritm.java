package control.algorithms;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import data.structure.interfaces.Graph;

/*
 *   Classe que implementa o algoritmo de Brin Kerbosch
 */
@SuppressWarnings({ "unchecked", "rawtypes"})
public class BronKerboschAlgoritm {
	
	private Graph graph;
	
	private List<Set> clique;
	
	/*
	 *  Construtor da classe, onde a variavel clique tem por objetivo retornar todos os subgrafos completos (i.e., cliques)  
	 */
	
	public BronKerboschAlgoritm(Graph g){
		this.graph = g;
	}
	
	
	public Collection<Set> getAllMaximalCliques(){
		
		this.clique = new ArrayList<Set>();
		
		this.findCliques(new ArrayList(),new ArrayList(this.graph.getVertices()),new ArrayList());
		
		List<Set> demais_cliques = new ArrayList<Set>();
		
		for(Set s : this.clique){
			if(s.size() > 2){
				for(Object o : s){
					Set aux = new HashSet(s);
					aux.remove(o);
					demais_cliques.add(aux);
				}
			}
				
		}
		
		this.clique.addAll(demais_cliques);
		return this.clique;
		
	}
	
	private void findCliques(List potential_clique, List candidates, List already_found){
		
	        List candidates_array = new ArrayList(candidates);
	        
	        if (!end(candidates, already_found)) {
	        	
	            // For each candidate_node in candidates do.
	        	for(int i = 0 ; i < candidates_array.size(); i++){
	        	
	            	Object candidate = candidates_array.get(i);
	            	
	                List new_candidates = new ArrayList();
	                List new_already_found = new ArrayList();

	                // Move candidate node to potential_clique.
	                potential_clique.add(candidate);
	                candidates.remove(candidate);

	                // Create new_candidates by removing nodes in candidates not connected to candidate node.
	                for (Object new_candidate : candidates) {
	                    if (graph.isNeighbor(candidate, new_candidate))
	                    {
	                        new_candidates.add(new_candidate);
	                    }
	                }

	                // Create new_already_found by removing nodes in already_found not connected to candidate node.
	                for (Object new_found : already_found) {
	                    if (graph.isNeighbor(candidate, new_found)) {
	                        new_already_found.add(new_found);
	                    }
	                }

	                // If new_candidates and new_already_found are empty
	                if (new_candidates.isEmpty() && new_already_found.isEmpty()) {
	                    // potential_clique is maximal_clique
	                    clique.add(new HashSet(potential_clique));
	                }
	                else {
	                    // Recursive call
	                    findCliques(potential_clique,new_candidates,new_already_found);
	                }

	                // Move candidate_node from potential_clique to already_found;
	                already_found.add(candidate);
	                potential_clique.remove(candidate);
	            }
	        }
	    }

		private boolean end(List candidates, List already_found){
	    	
	        // If a node in already_found is connected to all nodes in candidates
	        boolean end = false;
	        int edgecounter;
	        for (Object found : already_found) {
	            edgecounter = 0;
	            for (Object candidate : candidates) {
	                if (graph.isNeighbor(found,candidate)) {
	                    edgecounter++;
	                }
	            }
	            if (edgecounter == candidates.size()) {
	                end = true;
	                break;
	            }
	        }
	        
	        return end;
	    }

}
