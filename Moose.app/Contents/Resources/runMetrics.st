| stream model msePath eContext |

msePath := (CommandLine uniqueInstance allParameters) at: 2.
csvName := (CommandLine uniqueInstance allParameters) at: 3.
model := MooseModel new importFromMSEStream: (StandardFileStream readOnlyFileFullyNamed: msePath).
stream := MultiByteFileStream newFileNamed: csvName.

eContext := PropertyExportingContext new 
				separator: $,; 
				selectors: #(#fanIn #fanOut #numberOfAttributes #numberOfAttributesInherited #numberOfLinesOfCode #numberOfMethods #numberOfMethodsInherited #numberOfPrivateAttributes #numberOfPrivateMethods #numberOfPublicAttributes #numberOfPublicMethods #weightedMethodCount #hierarchyNestingLevel #numberOfChildren #lackOfCohesionInMethods #couplingBetweenClasses #responseForClass).

(((model allModelClasses, model allParameterizableClasses reject:[:each | each isStub or: [each isAnonymous]]) reject: #isAnonymous) reject: [:class | (class methods select: [:method | (method name = '<Initializer>') not and: [method isStub]]) notEmpty]) exportPropertyOnStream: stream context: eContext.
stream close.

WorldState addDeferredUIMessage: [SmalltalkImage current snapshot: false andQuit: true ].


