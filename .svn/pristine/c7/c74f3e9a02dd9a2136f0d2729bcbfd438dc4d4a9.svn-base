package gui;

import gui.analysis.AnalysisView;
import gui.cluster.ClusteringView;
import gui.metrics.MetricsExtractionView;

import java.awt.Frame;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import util.Properties;

/**
 *
 * @author hudson, gustavojss
 */
public class MainView extends javax.swing.JFrame {

	private static final long serialVersionUID = -7144305228539220119L;
	
	public MainView() {
        initComponents();
        initListeners();
        
        verificarConfiguracoes();
    }

    private void initComponents() {

    	setTitle("Verifica\u00E7\u00E3o de semelhan\u00E7a entre sistemas");
        desktop = new javax.swing.JDesktopPane();
        
        jMenuBar1 = new javax.swing.JMenuBar();
        menuArquivo = new javax.swing.JMenu();
        menuArquivo.setMnemonic('A');
        menuArquivo.setText("Arquivo");
        
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));
        
        configurarPastaMenuItem = new javax.swing.JMenuItem();
        extrairMetricasMenuItem = new javax.swing.JMenuItem();
        clusterizarMenuItem = new javax.swing.JMenuItem();
        analisarMenuItem = new javax.swing.JMenuItem();
        sairMenuItem = new javax.swing.JMenuItem();
        menuAjuda = new javax.swing.JMenu();

        configurarPastaMenuItem.setText("Configurar Pasta de Trabalho");
        extrairMetricasMenuItem.setText("Extrair M\u00E9tricas");
        clusterizarMenuItem.setText("Clusterizar");
        analisarMenuItem.setText("Analisar Resultados");
        sairMenuItem.setText("Sair");
        
        sairMenuItem.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        
        menuAjuda.setMnemonic('j');
        menuAjuda.setText("Ajuda");
        
        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setExtendedState(Frame.MAXIMIZED_BOTH);
        setFont(new java.awt.Font("DejaVu Sans", 0, 12)); // NOI18N
        setLocationByPlatform(true);
        getContentPane().setLayout(new java.awt.GridLayout(1, 0));
        getContentPane().add(desktop);

        menuArquivo.add(configurarPastaMenuItem);
        menuArquivo.add(extrairMetricasMenuItem);
        menuArquivo.add(clusterizarMenuItem);
        menuArquivo.add(analisarMenuItem);
        menuArquivo.add(jSeparator1);
        menuArquivo.add(sairMenuItem);

        jMenuBar1.add(menuArquivo);
        jMenuBar1.add(menuAjuda);

        setJMenuBar(jMenuBar1);
    }
    
    private void initListeners() {
    	configurarPastaMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuConfigurarPastaActionPerformed(evt);
            }
        });
    	
    	extrairMetricasMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExtrairMetricasActionPerformed(evt);
            }
        });
    	
    	clusterizarMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuClusterizarActionPerformed(evt);
            }
        });
    	
    	analisarMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuAnalisarActionPerformed(evt);
            }
        });
    	
    	sairMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuSairActionPerformed(evt);
            }
        });
    }

    private void menuConfigurarPastaActionPerformed(java.awt.event.ActionEvent evt) {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setMultiSelectionEnabled(false);

        if (chooser.showDialog(this, "Open") != JFileChooser.CANCEL_OPTION) {
        	Properties.setProperty(Properties.WORKSPACE, chooser.getSelectedFile().getAbsolutePath());
        	habilitarBotoes(true);
        	JOptionPane.showMessageDialog(this, "Pasta de Trabalho configurada com sucesso.", "", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void menuExtrairMetricasActionPerformed(java.awt.event.ActionEvent evt) {
        MetricsExtractionView extractionView = new MetricsExtractionView();
        this.desktop.add(extractionView);
        System.gc();
    }

    private void menuClusterizarActionPerformed(java.awt.event.ActionEvent evt) {
    	ClusteringView clusteringView = new ClusteringView();
        this.desktop.add(clusteringView);
        System.gc();
    }

    private void menuAnalisarActionPerformed(java.awt.event.ActionEvent evt) {
    	AnalysisView analysisView = new AnalysisView();
        this.desktop.add(analysisView);
        System.gc();
    }

    private void menuSairActionPerformed(java.awt.event.ActionEvent evt) {
        System.gc();
        System.exit(0);
    }
    
    private void verificarConfiguracoes() {
    	Properties.carregar();
    	
    	if (Properties.getProperty(Properties.WORKSPACE) == null)
    		habilitarBotoes(false);
    }
    
    private void habilitarBotoes(boolean enable) {
    	extrairMetricasMenuItem.setEnabled(enable);
		clusterizarMenuItem.setEnabled(enable);
		analisarMenuItem.setEnabled(enable);
    }

    public static void main(String args[]) {
    	
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainView().setVisible(true);
            }
        });
    }
    
    private javax.swing.JDesktopPane desktop;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenu menuAjuda;
    private javax.swing.JMenu menuArquivo;
    private javax.swing.JMenuItem configurarPastaMenuItem;
    private javax.swing.JMenuItem extrairMetricasMenuItem;
    private javax.swing.JMenuItem clusterizarMenuItem;
    private javax.swing.JMenuItem analisarMenuItem;
    private javax.swing.JMenuItem sairMenuItem;
    
}