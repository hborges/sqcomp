package gui.cluster;

import java.io.File;
import java.io.FileFilter;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JOptionPane;

import util.Properties;
import control.cluster.ClusteringController;

/**
 *
 * @author hudson, gustavojss
 */
public class ClusteringView extends javax.swing.JInternalFrame {

	private static final long serialVersionUID = 148033325706574967L;
	
	private Thread thread;
    private ClusteringController controller;
    private Set<Integer> selectedMetrics;
    
    private String clusteringInputFolder;
    private String clusteringPrimaryFolder, clusteringSecondaryFolder, clusteringMatricesFolder;

    public ClusteringView() {
        initComponents();
        initListeners();
        
        String workspaceFolder = Properties.getProperty(Properties.WORKSPACE) + File.separator; 
        this.clusteringInputFolder = workspaceFolder + Properties.METRICS_OUTPUT;
        retrieveProjects();
        
        this.pack();
        this.selectedMetrics = new HashSet<Integer>();
        
        String clusteringOutputFolder = workspaceFolder + Properties.CLUSTERING_OUTPUT + File.separator;
        this.clusteringPrimaryFolder = clusteringOutputFolder + Properties.CLUSTERING_PRIMARY_OUTPUT;
        this.clusteringSecondaryFolder = clusteringOutputFolder + Properties.CLUSTERING_SECONDARY_OUTPUT;
        this.clusteringMatricesFolder = clusteringOutputFolder + Properties.CLUSTERING_MATRICES_OUTPUT;
    }

    @SuppressWarnings({ "unchecked", "rawtypes" })
    private void initComponents() {

    	setTitle("Clusterizar Dados");
    	setClosable(true);
    	
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        nonSelectedProjects = new javax.swing.JList(new DefaultListModel<File>());
        nonSelectedProjects.setToolTipText("Aqui est�o listados os arquivos de m\u00E9tricas (.csv) contidos na pasta \"metricas\" da sua pasta de trabalho.");
        jScrollPane2 = new javax.swing.JScrollPane();
        selectedProjects = new javax.swing.JList(new DefaultListModel<File>());
        
        addAllProjectsButton = new javax.swing.JButton(">>");
        addProjectButton = new javax.swing.JButton(">");
        removeProjectButton = new javax.swing.JButton("<");
        removeAllProjectsButton = new javax.swing.JButton("<<");
        
        executarClusterizacaoButton = new javax.swing.JButton("Executar");
        executarClusterizacaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/start.png")));
        jPanel2 = new javax.swing.JPanel();
        
        allM = new javax.swing.JCheckBox("Selecionar Todas");
        loc = new javax.swing.JCheckBox("LoC (Linhas de Codigo)");
        nom = new javax.swing.JCheckBox("NoM (Numero de Metodos)");
        pubm = new javax.swing.JCheckBox("PubM (Metodos Publicos)");
        prim = new javax.swing.JCheckBox("PriM (Metodos Privados)");
        noa = new javax.swing.JCheckBox("NoA (Numero de Atributos)");
        puba = new javax.swing.JCheckBox("PubA (Atributos Publicos)");
        pria = new javax.swing.JCheckBox("PriA (Atributos Privados)");
        noah = new javax.swing.JCheckBox("NoAH (Atributos Herdados)");
        nomh = new javax.swing.JCheckBox("NoMH (Metodos Herdados)");
        
        String tooltip = "Valor que determina a partir de quantas m\u00E9tricas dois projetos s\u00E3o considerados similares.";
        jLabel3 = new javax.swing.JLabel("Valor p/ normalizar");
        jComboBox1 = new javax.swing.JComboBox();
        jLabel3.setToolTipText(tooltip);
        jComboBox1.setToolTipText(tooltip);
        
        try {
            setSelected(true);
        } catch (java.beans.PropertyVetoException e1) {
            e1.printStackTrace();
        }
        setVisible(true);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(
        		new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true), " Selecione os projetos a serem clusterizados "));

        jScrollPane1.setViewportView(nonSelectedProjects);
        jScrollPane2.setViewportView(selectedProjects);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addAllProjectsButton, javax.swing.GroupLayout.DEFAULT_SIZE, 53, Short.MAX_VALUE)
                    .addComponent(addProjectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(removeProjectButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addComponent(removeAllProjectsButton, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jScrollPane1, jScrollPane2});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(addAllProjectsButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(addProjectButton)
                        .addGap(73, 73, 73)
                        .addComponent(removeProjectButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(removeAllProjectsButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(
        		new javax.swing.border.LineBorder(new java.awt.Color(0, 0, 0), 2, true), " Selecione as m\u00E9tricas "));

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new Integer[] {}));
        jComboBox1.setEnabled(false);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(allM)
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jComboBox1, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(nom, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pubm, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(prim, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(puba, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(pria, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(nomh, javax.swing.GroupLayout.Alignment.LEADING))
                                .addGap(0, 2, Short.MAX_VALUE)))
                        .addGap(10, 10, 10))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(noah)
                            .addComponent(loc)
                            .addComponent(noa))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(allM)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(loc)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nom)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pubm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(prim)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(nomh)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(noa)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(puba)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pria)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(noah)
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(86, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(executarClusterizacaoButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(executarClusterizacaoButton))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        java.awt.Dimension dialogSize = getSize();
        setLocation((screenSize.width-dialogSize.width)/2,(screenSize.height-dialogSize.height)/2);
    }

    private void initListeners() {

    	// ------------------------------- Sele��o de projetos -------------------------------
        addAllProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAllProjectsActionPerformed(evt);
            }
        });
        
        addProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addProjectsActionPerformed(evt);
            }
        });
        
        removeProjectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeProjectsActionPerformed(evt);
            }
        });
        
        removeAllProjectsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                removeAllProjectsActionPerformed(evt);
            }
        });
    	
        // ------------------------------- Sele��o das m�tricas -------------------------------
        allM.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                allMetricsItemStateChanged(evt);
            }
        });
        
        loc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                locItemStateChanged(evt);
            }
        });

        nom.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nomItemStateChanged(evt);
            }
        });

        pubm.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                pubmItemStateChanged(evt);
            }
        });

        prim.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                primItemStateChanged(evt);
            }
        });
        
        noa.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                noaItemStateChanged(evt);
            }
        });

        puba.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                pubaItemStateChanged(evt);
            }
        });

        pria.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                priaItemStateChanged(evt);
            }
        });
        
        noah.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                noahItemStateChanged(evt);
            }
        });

        nomh.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                nomhItemStateChanged(evt);
            }
        });
        
        // ------------------------------- Executar Clusteriza��o -------------------------------
        executarClusterizacaoButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                executarClusterizacaoActionPerformed(evt);
            }
        });
    }

    @SuppressWarnings("unchecked")
	private void addAllProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	DefaultListModel<File> model = (DefaultListModel<File>) nonSelectedProjects.getModel();
    	File[] addedProjects = new File[model.size()];
    	
    	for (int i = 0; i < addedProjects.length; i++)
			addedProjects[i] = model.get(i);
    	
    	DefaultListModel<File> selectedModel = (DefaultListModel<File>) selectedProjects.getModel();
    	for (File project : addedProjects)
    		selectedModel.addElement(project);
    	
    	model.clear();
    }
    
    @SuppressWarnings("unchecked")
	private void addProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	int[] selection = nonSelectedProjects.getSelectedIndices();
    	
    	DefaultListModel<File> model = (DefaultListModel<File>) nonSelectedProjects.getModel();
    	File[] addedProjects = new File[selection.length];
    	
    	for (int i = 0; i < selection.length; i++)
			addedProjects[i] = model.get(selection[i]);
    	
    	DefaultListModel<File> selectedModel = (DefaultListModel<File>) selectedProjects.getModel();
    	for (File project : addedProjects)
    		selectedModel.addElement(project);
    	
    	for (int i = 0; i < selection.length; i++)
			model.remove(selection[i]);
    }
    
    @SuppressWarnings("unchecked")
	private void removeProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	int[] selection = selectedProjects.getSelectedIndices();
    	
    	DefaultListModel<File> model = (DefaultListModel<File>) selectedProjects.getModel();
    	File[] removedProjects = new File[selection.length];
    	
    	for (int i = 0; i < selection.length; i++)
			removedProjects[i] = model.get(selection[i]);
    	
    	DefaultListModel<File> selectedModel = (DefaultListModel<File>) nonSelectedProjects.getModel();
    	for (File project : removedProjects)
    		selectedModel.addElement(project);
    	
    	for (int i = 0; i < selection.length; i++)
			model.remove(selection[i]);
    }
    
    @SuppressWarnings("unchecked")
	private void removeAllProjectsActionPerformed(java.awt.event.ActionEvent evt) {
    	DefaultListModel<File> model = (DefaultListModel<File>) selectedProjects.getModel();
    	File[] removedProjects = new File[model.size()];
    	
    	for (int i = 0; i < removedProjects.length; i++)
			removedProjects[i] = model.get(i);
    	
    	DefaultListModel<File> selectedModel = (DefaultListModel<File>) nonSelectedProjects.getModel();
    	for (File project : removedProjects)
    		selectedModel.addElement(project);
    	
    	model.clear();
    }
    
    private void allMetricsItemStateChanged(java.awt.event.ItemEvent evt) {
        loc.setSelected(allM.isSelected());
        nom.setSelected(allM.isSelected());
        pubm.setSelected(allM.isSelected());
        prim.setSelected(allM.isSelected());
        nomh.setSelected(allM.isSelected());
        noa.setSelected(allM.isSelected());
        puba.setSelected(allM.isSelected());
        pria.setSelected(allM.isSelected());
        noah.setSelected(allM.isSelected());
        
        updateCheckBox();
    }

    private void locItemStateChanged(java.awt.event.ItemEvent evt) {
        if (loc.isSelected()) {
            this.selectedMetrics.add(ClusteringController.LOC);
        } else {
            this.selectedMetrics.remove(ClusteringController.LOC);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void nomItemStateChanged(java.awt.event.ItemEvent evt) {
        if (nom.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOM);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOM);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void pubmItemStateChanged(java.awt.event.ItemEvent evt) {
        if (pubm.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PUBM);
        } else {
            this.selectedMetrics.remove(ClusteringController.PUBM);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void primItemStateChanged(java.awt.event.ItemEvent evt) {
        if (prim.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PRIM);
        } else {
            this.selectedMetrics.remove(ClusteringController.PRIM);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void nomhItemStateChanged(java.awt.event.ItemEvent evt) {
        if (nomh.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOMH);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOMH);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void noaItemStateChanged(java.awt.event.ItemEvent evt) {
        if (noa.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOA);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOA);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void pubaItemStateChanged(java.awt.event.ItemEvent evt) {
        if (puba.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PUBA);
        } else {
            this.selectedMetrics.remove(ClusteringController.PUBA);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void priaItemStateChanged(java.awt.event.ItemEvent evt) {
        if (pria.isSelected()) {
            this.selectedMetrics.add(ClusteringController.PRIA);
        } else {
            this.selectedMetrics.remove(ClusteringController.PRIA);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }

    private void noahItemStateChanged(java.awt.event.ItemEvent evt) {
        if (noah.isSelected()) {
            this.selectedMetrics.add(ClusteringController.NOAH);
        } else {
            this.selectedMetrics.remove(ClusteringController.NOAH);
            this.allM.setSelected(false);
        }
        updateCheckBox();
    }
    
    @SuppressWarnings("unchecked")
	private void executarClusterizacaoActionPerformed(java.awt.event.ActionEvent evt) {
    	StringBuilder errorMessage = new StringBuilder();
    	
    	int valorNormalizacao = jComboBox1.getSelectedIndex() + 1;
    	if (valorNormalizacao == 0)
    		errorMessage.append("* Selecione o valor de normaliza\u00E7\u00E3o\n");
    	
    	if (selectedProjects.getModel().getSize() < 5)
            errorMessage.append("* Selecione pelo menos 5 projetos\n");
    	
        if (selectedMetrics.isEmpty())
            errorMessage.append("* Selecione pelo menos uma m\u00E9trica\n");
        
        if (errorMessage.length() != 0) {
            JOptionPane.showMessageDialog(this, errorMessage.toString());
        } else {
        	
        	File primaryFolder = new File(clusteringPrimaryFolder);
        	if (!primaryFolder.exists()) primaryFolder.mkdirs();
        	
        	File secondaryFolder = new File(clusteringSecondaryFolder);
        	if (!secondaryFolder.exists()) secondaryFolder.mkdirs();
        	
        	File matricesFolder = new File(clusteringMatricesFolder);
        	if (!matricesFolder.exists()) matricesFolder.mkdirs();
        	
        	try {
        		
        		DefaultListModel<File> model = (DefaultListModel<File>) selectedProjects.getModel();
        		File[] projects = new File[model.size()];
        		for (int i = 0; i < model.size(); i++)
					projects[i] = model.getElementAt(i);
        		
                this.controller = new ClusteringController(projects, primaryFolder, secondaryFolder, matricesFolder, selectedMetrics, valorNormalizacao);
                
                executarClusterizacaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/wait.gif")));
                executarClusterizacaoButton.setText("Executando");
                executarClusterizacaoButton.setEnabled(false);
                
                thread = new Thread(this.controller);
                thread.start();
                
                new Thread(new PanelUpdator()).start();
                
                this.repaint();
	        } catch (Exception e) {
	        	JOptionPane.showMessageDialog(this, e.getLocalizedMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
	        }
        }
    }
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	private void updateCheckBox() {
    	int selectedMetrics = this.selectedMetrics.size();
    	Integer[] options = new Integer[selectedMetrics];
    	
    	for (int i = 0; i < options.length; i++)
			options[i] = i + 1;
    	
    	jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(options));
    	jComboBox1.setEnabled(selectedMetrics > 0);
    	if (selectedMetrics > 0) 
    		jComboBox1.setSelectedIndex(0);
    }
    
    @SuppressWarnings("unchecked")
	private void retrieveProjects() {
    	File metricsFolder = new File(this.clusteringInputFolder);
    	File[] projects = metricsFolder.listFiles(getMetricFileFilter());
    	
    	boolean existProjects = !metricsFolder.exists() || projects.length == 0;
    	habilitarCampos(!existProjects);
    	
    	if (existProjects) {
    		JOptionPane.showMessageDialog(this, "N\u00E3o \u00E9 poss\u00EDvel realizar clusteriza\u00E7\u00E3o pois n\u00E3o h\u00E1 arquivos" +
    				" .csv na pasta \"metricas\". Favor realizar a extra�\u00E3o de m\u00E9tricas.", "Erro", JOptionPane.ERROR_MESSAGE);
    	} else {
    		DefaultListModel<File> model = (DefaultListModel<File>) this.nonSelectedProjects.getModel();
    		for (File project : projects) 
    			model.addElement(project);
    	}
    }
    
    private static FileFilter getMetricFileFilter() {
    	return new FileFilter() {
			@Override
			public boolean accept(File pathname) {
			    if (pathname.getName().endsWith(".csv"))
			        return true;
			    return false;
			}
		};
    }
    
    private void habilitarCampos(boolean enable) {
        nonSelectedProjects.setEnabled(enable);
        selectedProjects.setEnabled(enable);
        
        addAllProjectsButton.setEnabled(enable);
        addProjectButton.setEnabled(enable);
        removeProjectButton.setEnabled(enable);
        removeAllProjectsButton.setEnabled(enable);
        
        executarClusterizacaoButton.setEnabled(enable);
        
        allM.setEnabled(enable);
        loc.setEnabled(enable);
        nom.setEnabled(enable);
        pubm.setEnabled(enable);
        prim.setEnabled(enable);
        noa.setEnabled(enable);
        puba.setEnabled(enable);
        pria.setEnabled(enable);
        noah.setEnabled(enable);
        nomh.setEnabled(enable);
    }

    private class PanelUpdator implements Runnable {
        @Override
        public void run() {

            while (thread != null && thread.isAlive()) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ClusteringView.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            executarClusterizacaoButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/start.png")));
            executarClusterizacaoButton.setText("Executar");
            executarClusterizacaoButton.setEnabled(true);
            
            JOptionPane.showMessageDialog(ClusteringView.this, "Projetos Clusterizados com sucesso.", "", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private javax.swing.JButton executarClusterizacaoButton;
    @SuppressWarnings("rawtypes")
    private javax.swing.JList nonSelectedProjects;
    @SuppressWarnings("rawtypes")
    private javax.swing.JList selectedProjects;
    private javax.swing.JButton addAllProjectsButton;
    private javax.swing.JButton addProjectButton;
    private javax.swing.JButton removeProjectButton;
    private javax.swing.JButton removeAllProjectsButton;
	private javax.swing.JComboBox<Integer> jComboBox1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JCheckBox loc;
    private javax.swing.JCheckBox allM;
    private javax.swing.JCheckBox noa;
    private javax.swing.JCheckBox noah;
    private javax.swing.JCheckBox nom;
    private javax.swing.JCheckBox nomh;
    private javax.swing.JCheckBox pria;
    private javax.swing.JCheckBox prim;
    private javax.swing.JCheckBox puba;
    private javax.swing.JCheckBox pubm;
    
}